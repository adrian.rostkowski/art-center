package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.AuctionDTO;
import com.artcenter.artcenter.repositories.AuctionRepository;
import junitparams.JUnitParamsRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(JUnitParamsRunner.class)
public class AuctionServiceTest {

    private AuctionService auctionService;
    private AuctionRepository auctionRepository = mock(AuctionRepository.class);
    private UserService userService = mock(UserService.class);
    private ImgService imgService = mock(ImgService.class);
    private UserBidService userBidService = mock(UserBidService.class);

    @Before
    public void init() {
        auctionService = new AuctionService(auctionRepository, userService, imgService, userBidService);
    }

    @Test
    public void should_make_new_auction_based_on_dto() {
        //given
        AuctionDTO auctionDTO = AuctionDTO.builder()
                .auctionId(1L)
                .auctionTime("123456")
                .autoBuy(new BigDecimal(100))
                .description("description")
                .imageId(1L)
                .minimumBid(new BigDecimal(1))
                .startingBid(new BigDecimal(30))
                .theHighestBid(new BigDecimal(35))
                .title("MyAuction")
                .totalCountOfSeconds("1234567")
                .typeOfCharacter("human")
                .typeOfImage("safe")
                .build();

        User user = User.builder()
                .login("OwnerName")
                .id(1L)
                .build();

        given(userService.findById(user.getId())).willReturn(Optional.of(user));
        //when
        Auction auction = auctionService.makeNewAuction(auctionDTO, user);
        //then
        assertThat(auction.getDescription()).isEqualTo(auctionDTO.getDescription());
        verify(userBidService).makeInitialUserBid(auction);
    }

    @Test
    public void should_return_auctions_by_type_of_character() {
        //given
        String typeOfCharacter = "human";
        Optional<List<Auction>> auctions = Optional.of(Arrays.asList(
                Auction.builder().typeOfCharacter(typeOfCharacter).build(),
                Auction.builder().typeOfCharacter(typeOfCharacter).build(),
                Auction.builder().typeOfCharacter(typeOfCharacter).build()
        ));
        given(auctionRepository.findByTypeOfCharacter(typeOfCharacter)).willReturn(auctions);
        //when
        Optional<List<Auction>> found = auctionService.findByTypeOfCharacter(typeOfCharacter);
        //then
        assertThat(found.get().get(0).getTypeOfCharacter()).isEqualTo(typeOfCharacter);
    }

}
