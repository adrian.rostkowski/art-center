package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.UserAvatar;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAvatarRepository extends CrudRepository<UserAvatar, Long> {

    Optional<UserAvatar> findById(Long imageId);
}
