package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.Img;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DBFileRepository extends CrudRepository<Img, String> {
    Optional<List<Img>> findByOwnerId(Long ownerId);
    Optional<List<Img>> findByCategoryAndOwnerId(String category, Long ownerId);
}
