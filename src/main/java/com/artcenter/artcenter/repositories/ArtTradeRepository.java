package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.ArtTrade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArtTradeRepository extends CrudRepository<ArtTrade, Long> {

    Optional<List<ArtTrade>> findAllByIsActive(boolean isActive);
}
