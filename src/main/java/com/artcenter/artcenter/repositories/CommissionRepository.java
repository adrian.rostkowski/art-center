package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.Commission;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CommissionRepository extends CrudRepository<Commission, Long> {
    Optional<List<Commission>> findByArtist(String artist);

    Optional<List<Commission>> findByWinner(String winner);
}
