package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.ArtTradeBid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtTradeBidRepository extends CrudRepository<ArtTradeBid, Long> {
}
