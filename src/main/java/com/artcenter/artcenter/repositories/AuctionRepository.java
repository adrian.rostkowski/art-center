package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.Auction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuctionRepository extends CrudRepository<Auction, Long> {
    Optional<List<Auction>> findAllByOwnerId(Long ownerId);

    List<Auction> findAll();

    Optional<List<Auction>> findAllByIsActive(boolean isActive);

    Optional<List<Auction>> findAllByOwnerIdAndIsActive(Long ownerId, boolean isActive);

    Optional<List<Auction>> findAllByIsActiveOrIsAuctionOverByAutoBuy(boolean isActive, boolean isAuctionOverByAutoBuy);

    Optional<List<Auction>> findByTypeOfCharacter(String typeOfCharacter);

    Optional<List<Auction>> findByTypeOfImage(String typeOfImage);

    Optional<List<Auction>> findByTypeOfCharacterAndTypeOfImage(String typeOfCharacter, String typeOfImage);
}
