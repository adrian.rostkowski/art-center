package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.ArtTradeImg;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtTradeImgRepository extends CrudRepository<ArtTradeImg, Long> {
}
