package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.FriendListToken;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FriendListTokenRepo extends CrudRepository<FriendListToken, Long> {
    Optional<FriendListToken> findByUserId(Long id);

    Optional<FriendListToken> findByUserName(String userName);
}
