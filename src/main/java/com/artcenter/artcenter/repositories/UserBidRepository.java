package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.UserBid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserBidRepository extends CrudRepository<UserBid, Long> {
}
