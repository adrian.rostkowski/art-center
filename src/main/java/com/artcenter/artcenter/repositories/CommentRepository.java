package com.artcenter.artcenter.repositories;

import com.artcenter.artcenter.model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
