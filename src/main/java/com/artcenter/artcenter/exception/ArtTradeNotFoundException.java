package com.artcenter.artcenter.exception;

public class ArtTradeNotFoundException extends RuntimeException {

    public ArtTradeNotFoundException(Long id) {
        super("Can not find Art Trade: id - " + id);
    }

    public ArtTradeNotFoundException(boolean isActive) {
        super("Can not find Art Trade: isActive - " + isActive);
    }
}
