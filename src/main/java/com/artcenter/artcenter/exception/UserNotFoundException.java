package com.artcenter.artcenter.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String filed, String value) {
        super("Can not find User : " + filed + " - " + value);
    }
}
