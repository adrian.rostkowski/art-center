package com.artcenter.artcenter.exception;

public class UserAvatarNotFoundException extends RuntimeException {
    public UserAvatarNotFoundException(Long id) {
        super("Can not find User Avatar : id - " + id);
    }
}
