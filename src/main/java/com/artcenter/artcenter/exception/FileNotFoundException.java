package com.artcenter.artcenter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FileNotFoundException extends RuntimeException {
    public FileNotFoundException(String message) {
        super(message);
    }

    public FileNotFoundException(String field, Long id) {
        super("Can not find file by " + field + " " + id);
    }

    public FileNotFoundException(String fieldOne, String varOne,
                                 String fieldTwo, Long varTwo) {
        super("Can not find File : " + fieldOne + " : " + varOne + ", " + fieldTwo + " : " + varTwo);
    }
}
