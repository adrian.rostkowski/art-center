package com.artcenter.artcenter.exception;

public class AuctionNotFoundException extends RuntimeException {

    public AuctionNotFoundException(Long id) {
        super("Can not find Auction: id : " + id);
    }

    public AuctionNotFoundException(String field, Long id) {
        super("Can not find Auction: " + field + " : " + id);
    }

    public AuctionNotFoundException(String field, boolean result) {
        super("Can not find Auction: " + field + " : " + result);
    }

    public AuctionNotFoundException(String fieldOne, boolean resultOne,
                                    String fieldTwo, boolean resultTwo) {
        super("Can not find Auction: " + fieldOne + " : " + resultOne + ", " + fieldTwo + " : " + resultTwo);
    }

    public AuctionNotFoundException(String field) {
        super("Can not find Auction by : " + field);
    }

    public AuctionNotFoundException(String fieldOne, String fieldTwo) {
        super("Can not find Auction by : " + fieldOne + ", " + fieldTwo);
    }
}
