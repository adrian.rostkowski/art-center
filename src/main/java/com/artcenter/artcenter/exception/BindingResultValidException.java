package com.artcenter.artcenter.exception;

public class BindingResultValidException extends RuntimeException {

    public BindingResultValidException(String errorMessage) {
        super(errorMessage);
    }
}
