package com.artcenter.artcenter.exception;

public class CommissionNotFoundException extends RuntimeException {

    public CommissionNotFoundException(String field) {
        super("Can not find Commission: " + field + " - " + field);
    }

    public CommissionNotFoundException(Long id) {
        super("Can not find Commission: id - " + id);
    }
}
