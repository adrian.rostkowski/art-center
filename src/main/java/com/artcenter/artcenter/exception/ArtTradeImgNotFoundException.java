package com.artcenter.artcenter.exception;

public class ArtTradeImgNotFoundException extends RuntimeException {
    public ArtTradeImgNotFoundException(Long id) {
        super("Can not find Art Trade Image: id - " + id);
    }
}
