package com.artcenter.artcenter.exception;

public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String fileName, Throwable cause) {
        super("Could not store file " + fileName + ". Please try again!", cause);
    }
}
