package com.artcenter.artcenter.annotation;

import com.artcenter.artcenter.model.dto.UserDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordCheckValidator implements ConstraintValidator<PasswordCheck, UserDTO> {
    public void initialize(PasswordCheck constraint) {
    }

    @Override
    public boolean isValid(UserDTO userDTO, ConstraintValidatorContext context) {
        String password1 = userDTO.getPassword1();
        String password2 = userDTO.getPassword2();
        return password1.equals(password2);
    }
}
