package com.artcenter.artcenter.annotation;

import com.artcenter.artcenter.service.UserService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class IsThisLoginShouldExistValidator implements ConstraintValidator<IsThisLoginShouldExistCheck, String> {

    private final UserService userService;
    private boolean value;

    public IsThisLoginShouldExistValidator(UserService userService) {
        this.userService = Objects.requireNonNull(userService, "userService must be not null");
    }

    @Override
    public void initialize(IsThisLoginShouldExistCheck value) {
        this.value = value.value();
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        if(value){
            return userService.isThisUserNameExist(name);
        }else {
            return !userService.isThisUserNameExist(name);
        }
    }
}
