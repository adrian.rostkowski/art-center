package com.artcenter.artcenter.annotation;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.BasicBid;
import com.artcenter.artcenter.service.AuctionService;
import com.artcenter.artcenter.service.UserBidService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.Objects;

public class MinimumBidIncreaseValidator implements ConstraintValidator<MinimumBidIncreaseCheck, BasicBid> {

    private final AuctionService auctionService;
    private final UserBidService userBidService;

    public MinimumBidIncreaseValidator(AuctionService auctionService, UserBidService userBidService) {
        this.auctionService = Objects.requireNonNull(auctionService, "auctionService must be not null");
        this.userBidService = Objects.requireNonNull(userBidService, "userBidService must be not null");
    }

    public void initialize(MinimumBidIncreaseCheck constraint) {
    }

    public boolean isValid(BasicBid bid, ConstraintValidatorContext context) {
        Auction auction = auctionService
                .findById(Long.parseLong(bid.getAuctionId()))
                .orElseThrow(() -> new AuctionNotFoundException(Long.parseLong(bid.getAuctionId())));

        BigDecimal minimumIncrease = auction.getMinimumBid();
        BigDecimal theHighestBid = userBidService.returnTheHighestBid(auction);
        BigDecimal actualBid = bid.getBidValue();
        BigDecimal difference = actualBid.subtract(theHighestBid);

        int result = difference.compareTo(minimumIncrease);

        return result >= 0;
    }
}
