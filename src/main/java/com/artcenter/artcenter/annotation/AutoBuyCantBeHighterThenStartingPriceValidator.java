package com.artcenter.artcenter.annotation;

import com.artcenter.artcenter.model.BasicAuction;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class AutoBuyCantBeHighterThenStartingPriceValidator implements ConstraintValidator<AutoBuyMustBeBiggerThenStartingPriceCheck, BasicAuction> {
    @Override
    public void initialize(AutoBuyMustBeBiggerThenStartingPriceCheck constraintAnnotation) {

    }

    @Override
    public boolean isValid(BasicAuction basicAuction, ConstraintValidatorContext constraintValidatorContext) {

        if (basicAuction.getAutoBuy().equals(new BigDecimal(0))) {
            return true;
        }
        BigDecimal autoBuy = basicAuction.getAutoBuy();
        BigDecimal startingPrice = basicAuction.getStartingBid();
        int result = startingPrice.compareTo(autoBuy);
        return result < 0;
    }
}
