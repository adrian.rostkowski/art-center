package com.artcenter.artcenter.annotation;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.UserBidDTO;
import com.artcenter.artcenter.service.AuctionService;
import com.artcenter.artcenter.service.UserBidService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class IsThisBidIsTheHighestValidator implements ConstraintValidator<IsThisBidIsTheHighestCheck, UserBidDTO> {

    private final UserBidService userBidService;
    private final AuctionService auctionService;

    public IsThisBidIsTheHighestValidator(UserBidService userBidService, AuctionService auctionService) {
        this.userBidService = Objects.requireNonNull(userBidService, "userBidService must be not null");
        this.auctionService = Objects.requireNonNull(auctionService, "auctionService must be not null");
    }

    public void initialize(IsThisBidIsTheHighestCheck constraint) {
    }

    @Override
    public boolean isValid(UserBidDTO userBidDTO, ConstraintValidatorContext constraintValidatorContext) {
        Auction auction = auctionService
                .findById(Long.parseLong(userBidDTO.getAuctionId()))
                .orElseThrow(() -> new AuctionNotFoundException(Long.parseLong(userBidDTO.getAuctionId())));

        List<UserBid> userBids = auction.getUserBids();
        BigDecimal valueToCompare = userBidDTO.getBidValue();

        int result = userBidService.compareToTheHighest(userBids, valueToCompare);

        return result >= 1;
    }
}
