package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@Table(name = "files")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class Img {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String fileName;
    private String fileType;
    @Lob
    private byte[] data;
    private Long ownerId;
    private String ownerName;
    private String category;
    @ManyToOne(fetch = FetchType.LAZY)
    private User userOwner;
}
