package com.artcenter.artcenter.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BasicBid {
    private BigDecimal bidValue;
    private String auctionId;
}
