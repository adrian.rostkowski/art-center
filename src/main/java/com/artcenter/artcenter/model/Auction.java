package com.artcenter.artcenter.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class Auction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String typeOfImage;
    private String typeOfCharacter;
    private Long imageId;
    private BigDecimal startingBid;
    private BigDecimal minimumBid;
    private BigDecimal autoBuy;
    @Builder.Default
    private boolean isAuctionOverByAutoBuy = false;
    private boolean isActive;
    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime debtEndDate;
    private String ownerName;
    @OneToMany(mappedBy = "auction", fetch = FetchType.LAZY)
    private List<UserBid> userBids = new ArrayList<>();
}
