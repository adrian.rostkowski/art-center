package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class ArtTradeBid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long imageId;
    private String description;
    private String categoryImg;

    @ManyToOne(fetch = FetchType.LAZY)
    private ArtTrade artTrade;
    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;
}
