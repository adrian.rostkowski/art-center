package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class UserBid implements Comparable<UserBid> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String bidOwner;
    private BigDecimal bidValue;
    @ManyToOne(fetch = FetchType.LAZY)
    private Auction auction;

    //make log for null in bid value
    @Override
    public int compareTo(UserBid o) {
        return this.bidValue.compareTo(o.bidValue) * -1;
    }
}
