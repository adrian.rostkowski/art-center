package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class ArtTrade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;
    private Long imageId;
    private String title;
    private LocalDateTime endDateOfArtTrade;
    private boolean isActive;
    private String categoryOfImage;
    private String typeOfCharacter;

    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;
    @OneToMany(mappedBy = "artTrade", fetch = FetchType.LAZY)
    private List<ArtTradeBid> artTradeBid = new ArrayList<>();
}
