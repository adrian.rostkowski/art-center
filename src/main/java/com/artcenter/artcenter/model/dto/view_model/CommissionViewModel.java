package com.artcenter.artcenter.model.dto.view_model;

import lombok.*;

import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class CommissionViewModel {
    private String artist;
    private String winner;
    private String description;
    private Long auctionId;
    private Long imageId;
    private List<CommissionCommentViewModel> comment;
}
