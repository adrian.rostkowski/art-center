package com.artcenter.artcenter.model.dto;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class RequestDTO {
    private String artistName;
    private String requestType;
}
