package com.artcenter.artcenter.model.dto;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class PanelDTO {

    private Long artistId;
    private String artistName;
    private String countOfAuctions;
    private Long openedCommissions;
}
