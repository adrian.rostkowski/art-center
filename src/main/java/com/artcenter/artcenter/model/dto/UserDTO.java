package com.artcenter.artcenter.model.dto;

import com.artcenter.artcenter.annotation.IsThisLoginShouldExistCheck;
import com.artcenter.artcenter.annotation.PasswordCheck;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
@PasswordCheck(message = "Passwords must be equals")
public class UserDTO {
    @NotEmpty(message = "login must be not empty")
    @Size(min = 5, message = "login should have at least 5 characters")
    @IsThisLoginShouldExistCheck(value = false, message = "This login exist")
    private String login;
    @NotEmpty(message = "password1 must be not empty")
    private String password1;
    @NotEmpty(message = "password2 must be not empty")
    private String password2;
    @NotEmpty(message = "email must be not empty")
    @Email
    private String email;
    private String authenticationCode;
    private String authenticationCodeInput;
}
