package com.artcenter.artcenter.model.dto;

import com.artcenter.artcenter.annotation.AutoBuyMustBeBiggerThenStartingPriceCheck;
import com.artcenter.artcenter.model.BasicAuction;
import com.artcenter.artcenter.model.UserBid;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
@AutoBuyMustBeBiggerThenStartingPriceCheck(message = "Auto buy can't be higher then starting price")
public class AuctionDTO extends BasicAuction {
    @NotEmpty
    @Size(min = 1, max = 30, message = "Title must have more then 1 character and less then 30 characters")
    private String title;
    @NotEmpty
    @Size(max = 3000, message = "Description must have less then 300 characters")
    private String description;
    @NotEmpty
    private String typeOfImage;
    @NotEmpty
    private String typeOfCharacter;
    private MultipartFile file;
    @DecimalMin(value = "1.0", message = "Starting bid value cant lower then 1")
    @NotNull
    private BigDecimal startingBid;
    @DecimalMin(value = "1.0", message = "Minimum bid value cant lower then 1")
    @NotNull
    private BigDecimal minimumBid;
    @DecimalMin(value = "0.0", message = "AutoBuy value cant lower then 0")
    @NotNull
    private BigDecimal autoBuy;
    private Long imageId;
    private String auctionTime;
    private String ownerName;
    private String totalCountOfSeconds;
    private List<UserBid> userBids = new ArrayList<>();
    private BigDecimal theHighestBid;
    private Long auctionId;
}
