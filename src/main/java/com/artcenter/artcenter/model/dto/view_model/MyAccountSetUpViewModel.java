package com.artcenter.artcenter.model.dto.view_model;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class MyAccountSetUpViewModel {
    long countOfCommissions;
    long countOfWonAuctions;
}
