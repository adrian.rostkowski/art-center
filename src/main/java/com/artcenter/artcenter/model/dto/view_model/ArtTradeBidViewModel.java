package com.artcenter.artcenter.model.dto.view_model;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class ArtTradeBidViewModel {

    private Long imageId;
    private String description;
    private Long ownerId;
    private String ownerName;
    private String categoryImg;
}
