package com.artcenter.artcenter.model.dto;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter
@Getter
public class FullAuction {
    AuctionDTO auctionDTO;
    MultipartFile file;
}
