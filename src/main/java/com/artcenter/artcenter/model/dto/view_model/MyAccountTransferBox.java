package com.artcenter.artcenter.model.dto.view_model;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class MyAccountTransferBox {
    private Long transferObjectId;
    private Long imageId;
}
