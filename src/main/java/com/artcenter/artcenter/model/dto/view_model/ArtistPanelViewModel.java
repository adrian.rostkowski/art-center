package com.artcenter.artcenter.model.dto.view_model;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor()
@NoArgsConstructor()
@Setter
@Getter
public class ArtistPanelViewModel {
    private Long artistId;
    private Long avatarId;
    private Long totalCountOfOpenedCommissions;
    private Long totalCountOfAllAuctions;
    private Long totalCountOfActiveAuctions;
}
