package com.artcenter.artcenter.model.dto.view_model;

import com.artcenter.artcenter.annotation.AutoBuyMustBeBiggerThenStartingPriceCheck;
import com.artcenter.artcenter.model.BasicAuction;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
@AutoBuyMustBeBiggerThenStartingPriceCheck(message = "Auto but can't be higher then starting price")
public class AuctionAngularDTO extends BasicAuction {
    @NotEmpty
    @Size(min = 1, max = 30, message = "Title must have more then 1 character and less then 30 characters")
    private String title;
    private Long imageId;
    @NotNull
    private BigDecimal highestBid;
    private Long auctionId;
}
