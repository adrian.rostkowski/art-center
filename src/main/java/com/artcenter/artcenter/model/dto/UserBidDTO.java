package com.artcenter.artcenter.model.dto;

import com.artcenter.artcenter.annotation.IsThisBidIsTheHighestCheck;
import com.artcenter.artcenter.annotation.MinimumBidIncreaseCheck;
import com.artcenter.artcenter.model.BasicBid;
import lombok.*;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@IsThisBidIsTheHighestCheck(message = "This bid is too low")
@MinimumBidIncreaseCheck(message = "Too low bid")
public class UserBidDTO extends BasicBid {
    @DecimalMax(value = "1000000000.0", message = "Max debt value cant be bigger then 1 000 000 000")
    @DecimalMin(value = "1.0", message = "Debt value cant lower then 1")
    @Builder.Default
    private BigDecimal bidValue = new BigDecimal(0);
    private String auctionId;
    private String bidOwner;
    @Builder.Default
    private boolean isAuctionActive = true;
}
