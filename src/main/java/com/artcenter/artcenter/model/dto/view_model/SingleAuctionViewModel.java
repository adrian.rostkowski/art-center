package com.artcenter.artcenter.model.dto.view_model;

import com.artcenter.artcenter.annotation.AutoBuyMustBeBiggerThenStartingPriceCheck;
import com.artcenter.artcenter.model.BasicAuction;
import com.artcenter.artcenter.model.dto.UserBidDTO;
import lombok.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
@AutoBuyMustBeBiggerThenStartingPriceCheck(message = "Auto but must be higher then starting price")
public class SingleAuctionViewModel extends BasicAuction {
    @NotEmpty
    @Size(min = 1, max = 30, message = "Title must have more then 1 character and less then 30 characters")
    private String title;
    @NotEmpty
    @Size(max = 3000, message = "Description must have less then 300 characters")
    private String description;
    @DecimalMin(value = "1.0", message = "Debt value cant lower then 1")
    @NotNull
    private BigDecimal autoBuy;
    @NotNull
    private BigDecimal startingBid;
    @NotNull
    private BigDecimal minimumBid;
    private Long imageId;
    private String auctionTime;
    private String ownerName;
    private String totalCountOfSeconds;
    private BigDecimal theHighestBid;
    private boolean active;
    private Long ownerId;
    private List<UserBidDTO> userBidViewModelList;
}
