package com.artcenter.artcenter.model.dto.view_model;

import lombok.*;

import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class ArtTradeViewModel {

    private Long artTradeId;
    private String description;
    private String categoryImg;
    private String typeOfCharacter;
    private Long imageId;
    private String title;
    private String totalCountOfSeconds;
    private Long countOfDays;
    private Long ownerId;
    private String ownerName;

    private List<ArtTradeBidViewModel> artTradeBidViewModelList;
}
