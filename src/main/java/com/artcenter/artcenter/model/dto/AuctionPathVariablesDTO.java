package com.artcenter.artcenter.model.dto;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class AuctionPathVariablesDTO {
    @Builder.Default
    private String type = "all";
    @Builder.Default
    private String category = "all";
    @Builder.Default
    private String mainTypeOfAuction = "ych";
}
