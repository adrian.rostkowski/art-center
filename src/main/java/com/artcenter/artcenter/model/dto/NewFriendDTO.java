package com.artcenter.artcenter.model.dto;

import com.artcenter.artcenter.annotation.IsThisLoginShouldExistCheck;
import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class NewFriendDTO {
    @IsThisLoginShouldExistCheck(value = true, message = "This user doesn't exist")
    private String name;
}
