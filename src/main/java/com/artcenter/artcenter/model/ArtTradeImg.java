package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@Table(name = "ART_TRADE_IMG")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class ArtTradeImg {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fileName;
    private String fileType;
    @Lob
    private byte[] data;
    private Long ownerId;
    private String ownerName;
    private String category;
    private String typeOfCharacter;
    private String purpose;
    @ManyToOne(fetch = FetchType.LAZY)
    private User artTradeOwnerId;
}
