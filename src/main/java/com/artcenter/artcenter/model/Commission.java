package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class Commission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String artist;
    private String winner;
    private boolean isActive;
    @OneToOne
    @JoinColumn(name = "auction_id")
    private Auction auction;
    @OneToMany(mappedBy = "commission", fetch = FetchType.LAZY)
    private List<Comment> comments;
}
