package com.artcenter.artcenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
@Entity
@Builder(toBuilder = true)
@Table(name = "USER")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;
    private String password;
    @Builder.Default
    private Integer active = 0;
    @Builder.Default
    private String permissions = "";
    private String email;
    private String authenticationCode;
    private String authenticationCodeInput;

    @OneToOne
    @JoinColumn(name = "user_avatar", referencedColumnName = "id")
    private UserAvatar userAvatar;

    @OneToMany(mappedBy = "commission", fetch = FetchType.LAZY)
    private List<Comment> comment;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_friends",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "friendUser_id", referencedColumnName = "id"))
    private Set<User> friendsList;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_friends",
            joinColumns = @JoinColumn(
                    name = "friendUser_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"))
    private List<User> friendOf;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "invite_lists",
            joinColumns = @JoinColumn(
                    name = "user1_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "user2_id", referencedColumnName = "userId"))
    private Set<FriendListToken> invitesToFriendListSet;

    @OneToMany(mappedBy = "userOwner", fetch = FetchType.LAZY)
    private List<Img> imgList;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<ArtTrade> artTradeList;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<Auction> ychList;

    @OneToMany(mappedBy = "artTradeOwnerId", fetch = FetchType.LAZY)
    private List<ArtTradeImg> artTradeImgList;
}
