package com.artcenter.artcenter.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@Table(name = "USER_AVATAR")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class UserAvatar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long ownerId;
    @Lob
    private byte[] data;
    @OneToOne(mappedBy = "userAvatar")
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
}
