package com.artcenter.artcenter.model;

import lombok.*;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter
@Getter
public class UserDataToSetHeaders {

    private String userId;
    private String userAvatarId;
}
