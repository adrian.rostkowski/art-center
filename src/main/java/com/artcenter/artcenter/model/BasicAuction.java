package com.artcenter.artcenter.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BasicAuction {
    protected BigDecimal autoBuy;
    protected BigDecimal startingBid;
}
