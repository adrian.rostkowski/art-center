package com.artcenter.artcenter.model;

public interface RequestToArtistI {
    String getArtist();

    String getRequester();

    boolean isActive();

    int getImageId();
}
