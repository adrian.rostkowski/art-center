package com.artcenter.artcenter.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class LoginUser extends org.springframework.security.core.userdetails.User {

    public LoginUser(User user, List<GrantedAuthority> grantedAuthorityList) {
        super(user.getLogin(), user.getPassword(), grantedAuthorityList);
    }
}

