package com.artcenter.artcenter.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Objects;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    public WebSecurityConfig(@Qualifier("loginUserDetailsService") UserDetailsService userDetailsService) {
        this.userDetailsService = Objects.requireNonNull(userDetailsService, "userDetailsService must be not null");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                    .and()
                .headers()
                    .frameOptions()
                    .disable()
                .and()
                    .csrf()
                    .disable()
                .authorizeRequests()
                    .antMatchers(
                            "/",
                            "/home",
                            "/make-new-auction-angular",
                            "/upload-auction-model",
                            "/create-new-user",
                            "/all-auctions",
                            "/create-new-user-authentication",
                            "/all-auctions/*/*",
                            "/art-trade/get-by/id/*",
                            "/art-trades/*/art-trade-bids",
                            "/auction/by-id/*",
                            "/v2/api-docs",
                            "/swagger-ui.html",
                            //todo test
                            "/get-user-avatar/{imageId}",
                            "/art-trade-image/download/*",
                            "/single-auction/**",
                            "/downloadFile/*")
                            .permitAll()
                    .antMatchers("/art-center-db/**")
                            .permitAll()
                    .antMatchers("/user-panel")
                            .hasAnyRole("USER", "ADMIN")
                    .anyRequest()
                            .fullyAuthenticated()
                .and()
                    .httpBasic()
                .and()
                    .formLogin()
                        .loginPage("/login")
                        .permitAll()
                .and()
                    .logout()
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/home")
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
}
