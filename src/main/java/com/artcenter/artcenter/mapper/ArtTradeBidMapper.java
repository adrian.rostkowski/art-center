package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.ArtTradeBid;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeBidViewModel;
import org.springframework.stereotype.Component;

@Component
public class ArtTradeBidMapper implements Mapper<ArtTradeBid, ArtTradeBidViewModel> {
    @Override
    public ArtTradeBid toEntity(ArtTradeBidViewModel dto) {
        return null;
    }

    @Override
    public ArtTradeBidViewModel toDto(ArtTradeBid entity) {
        return ArtTradeBidViewModel.builder()
                .ownerName(entity.getOwner().getLogin())
                .ownerId(entity.getOwner().getId())
                .imageId(entity.getImageId())
                .description(entity.getDescription())
                .categoryImg(entity.getCategoryImg())
                .build();
    }
}
