package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.Comment;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.dto.view_model.CommissionCommentViewModel;
import com.artcenter.artcenter.model.dto.view_model.CommissionViewModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CommissionMapper implements Mapper<Commission, CommissionViewModel> {

    private final CommissionCommentMapper commissionCommentMapper;

    public CommissionMapper(CommissionCommentMapper commissionCommentMapper) {
        this.commissionCommentMapper = Objects.requireNonNull(commissionCommentMapper,
                "commissionCommentMapper must be not null");
    }

    @Override
    public Commission toEntity(CommissionViewModel dto) {
        return null;
    }

    @Override
    public CommissionViewModel toDto(Commission entity) {

        List<Comment> commentList = entity.getComments();
        List<CommissionCommentViewModel> commissionCommentViewModelList = commentList.stream()
                .map(commissionCommentMapper::toDto)
                .collect(Collectors.toList());

        return CommissionViewModel.builder()
                .description(entity.getAuction().getDescription())
                .imageId(entity.getAuction().getImageId())
                .artist(entity.getArtist())
                .auctionId(entity.getAuction().getId())
                .winner(entity.getWinner())
                .comment(commissionCommentViewModelList)
                .build();
    }
}
