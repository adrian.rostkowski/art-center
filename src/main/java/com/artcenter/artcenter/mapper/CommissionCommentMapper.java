package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.Comment;
import com.artcenter.artcenter.model.dto.view_model.CommissionCommentViewModel;
import org.springframework.stereotype.Component;

@Component
public class CommissionCommentMapper implements Mapper<Comment, CommissionCommentViewModel> {
    @Override
    public Comment toEntity(CommissionCommentViewModel dto) {
        return null;
    }

    @Override
    public CommissionCommentViewModel toDto(Comment entity) {
        return CommissionCommentViewModel.builder()
                .ownerName(entity.getUser().getLogin())
                .text(entity.getText())
                .build();
    }
}
