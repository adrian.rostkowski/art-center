package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.UserBidDTO;
import com.artcenter.artcenter.model.dto.view_model.SingleAuctionViewModel;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SingleAuctionMapper implements Mapper<Auction, SingleAuctionViewModel> {

    private final UserBidMapper userBidMapper;

    public SingleAuctionMapper(UserBidMapper userBidMapper) {
        this.userBidMapper = userBidMapper;
    }

    @Override
    public Auction toEntity(SingleAuctionViewModel dto) {
        return null;
    }

    @Override
    public SingleAuctionViewModel toDto(Auction auction) {

        List<UserBid> sortedList = mapHashSetToTreeSet(auction.getUserBids());
        BigDecimal theHighestBid = auction.getStartingBid();
        if (sortedList.size() != 0) {
            theHighestBid = sortedList.get(0).getBidValue();
        }

        List<UserBidDTO> userBidDTOS = auction.getUserBids().stream().map(userBidMapper::toDto).collect(Collectors.toList());

        return SingleAuctionViewModel.builder()
                .autoBuy(auction.getAutoBuy())
                .description(auction.getDescription())
                .imageId(auction.getImageId())
                .minimumBid(auction.getMinimumBid())
                .ownerName(auction.getOwnerName())
                .title(auction.getTitle())
                .theHighestBid(theHighestBid)
                .totalCountOfSeconds(calculateTotalCountOfSeconds(auction.getDebtEndDate()))
                .auctionTime(auction.getDebtEndDate().toString())
                .startingBid(auction.getStartingBid())
                .userBidViewModelList(userBidDTOS)
                .active(auction.isActive())
                .ownerId(auction.getOwner().getId())
                .build();
    }

    private String calculateTotalCountOfSeconds(LocalDateTime debtEndDate) {
        if (debtEndDate != null) {
            long diff = ChronoUnit.SECONDS.between(LocalDateTime.now(), debtEndDate);
            return String.valueOf(diff);
        } else return " ";
    }

    private List<UserBid> mapHashSetToTreeSet(List<UserBid> list) {
        Collections.sort(list);
        return list;
    }

}
