package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.ArtTrade;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeAuctionBox;
import org.springframework.stereotype.Component;

@Component
public class ArtTradeAuctionBoxMapper implements Mapper<ArtTrade, ArtTradeAuctionBox> {

    @Override
    public ArtTrade toEntity(ArtTradeAuctionBox dto) {
        return null;
    }

    @Override
    public ArtTradeAuctionBox toDto(ArtTrade entity) {
        return ArtTradeAuctionBox.builder()
                .artTradeId(entity.getId())
                .imageId(entity.getImageId())
                .build();
    }
}
