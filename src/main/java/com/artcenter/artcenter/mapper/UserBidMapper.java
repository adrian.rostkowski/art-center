package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.UserBidDTO;
import com.artcenter.artcenter.service.AuctionService;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserBidMapper implements Mapper<UserBid, UserBidDTO> {

    private final AuctionService auctionService;

    public UserBidMapper(AuctionService auctionService) {
        this.auctionService = Objects.requireNonNull(auctionService, "auctionService must be not null");
    }


    @Override
    public UserBid toEntity(UserBidDTO dto) {
        Auction auction = auctionService
                .findById(Long.parseLong(dto.getAuctionId()))
                .orElseThrow(() -> new AuctionNotFoundException(Long.parseLong(dto.getAuctionId())));

        return UserBid.builder()
                .bidValue(dto.getBidValue())
                .bidOwner(dto.getBidOwner())
                .auction(auction)
                .build();
    }

    @Override
    public UserBidDTO toDto(UserBid entity) {
        return UserBidDTO.builder()
                .auctionId(String.valueOf(entity.getId()))
                .bidOwner(entity.getBidOwner())
                .bidValue(entity.getBidValue())
                .isAuctionActive(entity.getAuction().isActive())
                .build();
    }

}
