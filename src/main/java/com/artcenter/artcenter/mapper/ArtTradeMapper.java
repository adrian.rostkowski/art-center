package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.ArtTrade;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeViewModel;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class ArtTradeMapper implements Mapper<ArtTrade, ArtTradeViewModel> {
    @Override
    public ArtTrade toEntity(ArtTradeViewModel artTradeBidViewModel) {
        return null;
    }

    @Override
    public ArtTradeViewModel toDto(ArtTrade artTrade) {
        return ArtTradeViewModel.builder()
                .artTradeId(artTrade.getId())
                .description(artTrade.getDescription())
                .imageId(artTrade.getImageId())
                .ownerId(artTrade.getOwner().getId())
                .ownerName(artTrade.getOwner().getLogin())
                .title(artTrade.getTitle())
                .totalCountOfSeconds(calculateTotalCountOfSeconds(artTrade.getEndDateOfArtTrade()))
                .build();
    }

    private String calculateTotalCountOfSeconds(LocalDateTime debtEndDate) {
        if (debtEndDate != null) {
            Long diff = ChronoUnit.SECONDS.between(LocalDateTime.now(), debtEndDate);
            return String.valueOf(diff);
        } else return " ";
    }
}
