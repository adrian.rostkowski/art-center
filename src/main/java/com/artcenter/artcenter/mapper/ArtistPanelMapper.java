package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.exception.CommissionNotFoundException;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.view_model.ArtistPanelViewModel;
import com.artcenter.artcenter.service.AuctionService;
import com.artcenter.artcenter.service.CommissionService;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ArtistPanelMapper implements Mapper<User, ArtistPanelViewModel> {

    private final AuctionService auctionService;
    private final CommissionService commissionService;

    public ArtistPanelMapper(AuctionService auctionService, CommissionService commissionService) {
        this.auctionService = Objects.requireNonNull(auctionService, "auctionService must be not null");
        this.commissionService = Objects.requireNonNull(commissionService, "commissionService must be not null");
    }

    @Override
    public User toEntity(ArtistPanelViewModel dto) {
        return null;
    }

    @Override
    public ArtistPanelViewModel toDto(User entity) {
        return ArtistPanelViewModel.builder()
                .artistId(entity.getId())
                .avatarId(entity.getUserAvatar().getId())
                .totalCountOfActiveAuctions(getCountOfActiveAuctions(entity.getId()))
                .totalCountOfAllAuctions(getCountOfAllUserAuctions(entity.getId()))
                .totalCountOfOpenedCommissions(getTotalCountOfOpenedCommissions(entity.getLogin()))
                .build();
    }

    private Long getCountOfActiveAuctions(Long userId) {
        try {
            return auctionService.returnAllByUserIdAndIsActive(userId, true).stream()
                    .count();
        } catch (AuctionNotFoundException ex) {
            return 0L;
        }
    }

    private Long getCountOfAllUserAuctions(Long userId) {
        try {
            return auctionService.returnAllUserAuctions(userId)
                    .orElseThrow(() -> new AuctionNotFoundException("ownerId", userId))
                    .stream()
                    .count();
        } catch (AuctionNotFoundException ex) {
            return 0L;
        }
    }

    private Long getTotalCountOfOpenedCommissions(String artistName) {
        try {
            return commissionService.findByArtistName(artistName).stream()
                    .count();
        } catch (CommissionNotFoundException ex) {
            return 0L;
        }
    }
}
