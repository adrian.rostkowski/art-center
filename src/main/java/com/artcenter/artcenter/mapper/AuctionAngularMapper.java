package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.view_model.AuctionAngularDTO;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Component
public class AuctionAngularMapper implements Mapper<Auction, AuctionAngularDTO> {
    @Override
    public Auction toEntity(AuctionAngularDTO dto) {
        return null;
    }

    @Override
    public AuctionAngularDTO toDto(Auction entity) {

        List<UserBid> sortedList = mapHashSetToTreeSet(entity.getUserBids());
        BigDecimal theHighestBid = entity.getStartingBid();
        if (sortedList.size() != 0) {
            theHighestBid = sortedList.get(0).getBidValue();
        }

        return AuctionAngularDTO.builder()
                .imageId(entity.getImageId())
                .title(entity.getTitle())
                .highestBid(theHighestBid)
                .auctionId(entity.getId())
                .build();
    }

    private List<UserBid> mapHashSetToTreeSet(List<UserBid> list) {
        Collections.sort(list);
        return list;
    }
}
