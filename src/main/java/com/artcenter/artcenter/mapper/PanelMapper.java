package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.exception.CommissionNotFoundException;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.PanelDTO;
import com.artcenter.artcenter.service.AuctionService;
import com.artcenter.artcenter.service.CommissionService;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PanelMapper implements Mapper<User, PanelDTO> {

    private final AuctionService auctionService;
    private final CommissionService commissionService;

    public PanelMapper(AuctionService auctionService, CommissionService commissionService) {
        this.auctionService = Objects.requireNonNull(auctionService, "auctionService must be not null");
        this.commissionService = Objects.requireNonNull(commissionService, "commissionService must be not null");
    }


    @Override
    public User toEntity(PanelDTO dto) {
        return null;
    }

    @Override
    public PanelDTO toDto(User user) {
        String countOfAuctions = String.valueOf(
                auctionService.returnAllUserAuctions(user.getId())
                        .orElseThrow(() -> new AuctionNotFoundException("ownerId", user.getId()))
                        .stream()
                        .filter(Auction::isActive)
                        .count()
        );

        Long countOfActiveCommissions = commissionService.findByArtistName(user.getLogin())
                .orElseThrow(() -> new CommissionNotFoundException("artistName"))
                .stream()
                .filter(Commission::isActive)
                .count();

        return PanelDTO.builder()
                .artistId(user.getId())
                .artistName(user.getLogin())
                .countOfAuctions(countOfAuctions)
                .openedCommissions(countOfActiveCommissions)
                .build();
    }
}
