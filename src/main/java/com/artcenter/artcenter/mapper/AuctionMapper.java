package com.artcenter.artcenter.mapper;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.AuctionDTO;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

@Component
public class AuctionMapper implements Mapper<Auction, AuctionDTO> {
    @Override
    public Auction toEntity(AuctionDTO dto) {
        return null;
    }

    @Override
    public AuctionDTO toDto(Auction auction) {

        List<UserBid> sortedList = mapHashSetToTreeSet(auction.getUserBids());
        BigDecimal theHighestBid = auction.getStartingBid();
        if (sortedList.size() != 0) {
            theHighestBid = sortedList.get(0).getBidValue();
        }

        return AuctionDTO.builder()
                .auctionId(auction.getId())
                .auctionTime(auction.getDebtEndDate().toString())
                .title(auction.getTitle())
                .description(auction.getDescription())
                .typeOfCharacter(auction.getTypeOfCharacter())
                .typeOfImage(auction.getTypeOfImage())
                .imageId(auction.getImageId())
                .startingBid(auction.getStartingBid())
                .minimumBid(auction.getMinimumBid())
                .autoBuy(auction.getAutoBuy())
                .ownerName(auction.getOwnerName())
                .totalCountOfSeconds(calculateTotalCountOfSeconds(auction.getDebtEndDate()))
                .userBids(mapHashSetToTreeSet(sortedList))
                .theHighestBid(theHighestBid)
                .build();
    }

    private String calculateTotalCountOfSeconds(LocalDateTime debtEndDate) {
        if (debtEndDate != null) {
            long diff = ChronoUnit.SECONDS.between(LocalDateTime.now(), debtEndDate);
            return String.valueOf(diff);
        } else return " ";
    }

    private List<UserBid> mapHashSetToTreeSet(List<UserBid> list) {
        Collections.sort(list);
        return list;
    }
}
