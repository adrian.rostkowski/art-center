package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.FileNotFoundException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.Img;
import com.artcenter.artcenter.model.dto.view_model.MyAccountTransferBox;
import com.artcenter.artcenter.service.ImgService;
import com.artcenter.artcenter.service.TransferBoxService;
import com.artcenter.artcenter.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
public class GalleryController {
    private static final String CATEGORY_GALLERY = "gallery";
    private final UserService userService;
    private final ImgService imgService;
    private final TransferBoxService transferBoxService;

    public GalleryController(UserService userService, ImgService imgService, TransferBoxService transferBoxService) {
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.imgService = Objects.requireNonNull(imgService,
                "imgService must be not null");
        this.transferBoxService = Objects.requireNonNull(transferBoxService,
                "transferBoxService must be not null");
    }

    @GetMapping("/gallery-images/user")
    public ResponseEntity<List<String>> getGalleryImagesByUserIdHeader(@RequestHeader("UserId") Long userId) {
        List<String> galleryImagesId = imgService.returnAllGalleryImages(userId)
                .orElseThrow(() -> new FileNotFoundException("userId", userId))
                .stream()
                .map(Img::getId)
                .collect(Collectors.toList());

        return new ResponseEntity<>(galleryImagesId, HttpStatus.OK);
    }

    @GetMapping("/gallery-images/artist-name/{artistName}")
    public ResponseEntity<List<MyAccountTransferBox>> getAllGalleryImagesByArtistName(@PathVariable String artistName) {
        Long userId = userService
                .findByLogin(artistName)
                .orElseThrow(() -> new UserNotFoundException("login", artistName))
                .getId();

        List<MyAccountTransferBox> myAccountTransferBoxes = getGalleryImagesIdByUserId(userId).stream()
                .map(transferBoxService::galleryImgIdToMyAccountTransferBox)
                .collect(Collectors.toList());

        return new ResponseEntity<>(myAccountTransferBoxes, HttpStatus.OK);
    }

    private List<String> getGalleryImagesIdByUserId(Long userId) {
        return imgService.returnAllGalleryImages(userId)
                .orElseThrow(() -> new FileNotFoundException("userId", userId))
                .stream()
                .map(Img::getId)
                .collect(Collectors.toList());
    }
}
