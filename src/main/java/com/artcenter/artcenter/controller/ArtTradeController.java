package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.ArtTradeNotFoundException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.mapper.ArtTradeAuctionBoxMapper;
import com.artcenter.artcenter.mapper.ArtTradeBidMapper;
import com.artcenter.artcenter.mapper.ArtTradeMapper;
import com.artcenter.artcenter.model.ArtTrade;
import com.artcenter.artcenter.model.ArtTradeBid;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeAuctionBox;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeBidViewModel;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeViewModel;
import com.artcenter.artcenter.service.ArtTradeService;
import com.artcenter.artcenter.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
public class ArtTradeController {

    private final ArtTradeService artTradeService;
    private final ArtTradeMapper artTradeMapper;
    private final UserService userService;
    private final ArtTradeBidMapper artTradeBidMapper;
    private final ArtTradeAuctionBoxMapper artTradeAuctionBoxMapper;

    public ArtTradeController(ArtTradeService artTradeService,
                              ArtTradeMapper artTradeMapper,
                              UserService userService,
                              ArtTradeBidMapper artTradeBidMapper,
                              ArtTradeAuctionBoxMapper artTradeAuctionBoxMapper) {

        this.artTradeService = Objects.requireNonNull(artTradeService,
                "artTradeService must be not null");
        this.artTradeMapper = Objects.requireNonNull(artTradeMapper,
                "artTradeMapper must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.artTradeBidMapper = Objects.requireNonNull(artTradeBidMapper,
                "artTradeBidMapper must be not null");
        this.artTradeAuctionBoxMapper = artTradeAuctionBoxMapper;
    }

    @GetMapping("/art-trade/get-by/id/{id}")
    public ResponseEntity<ArtTradeViewModel> getById(@PathVariable Long id) {

        ArtTrade foundArtTrade = artTradeService.getById(id)
                .orElseThrow(() -> new ArtTradeNotFoundException(id));

        ArtTradeViewModel found = artTradeMapper.toDto(foundArtTrade);

        return new ResponseEntity<>(found, HttpStatus.OK);
    }

    @GetMapping("/art-trades/all/all")
    public ResponseEntity<List<ArtTradeAuctionBox>> getAllActiveArtTrades() {
        List<ArtTradeAuctionBox> artTradeAuctionBoxes = artTradeService.getByIsActive(true)
                .orElseThrow(() -> new ArtTradeNotFoundException(true))
                .stream()
                .map(artTradeAuctionBoxMapper::toDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(artTradeAuctionBoxes, HttpStatus.OK);
    }

    @GetMapping("/art-trades/{category}/{type}")
    public ResponseEntity<List<ArtTradeAuctionBox>> getArtTrades(@PathVariable String category,
                                                                 @PathVariable String type) {

        List<ArtTradeAuctionBox> artTradeAuctionBoxes = artTradeService.getByIsActive(true)
                .orElseThrow(() -> new ArtTradeNotFoundException(true))
                .stream()
                .filter(auction -> auction.getTypeOfCharacter().equals(type))
                .filter(auction -> auction.getCategoryOfImage().equals(category))
                .map(artTradeAuctionBoxMapper::toDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(artTradeAuctionBoxes, HttpStatus.OK);
    }

    @GetMapping("/art-trades/{artTradeId}/art-trade-bids")
    public ResponseEntity<List<ArtTradeBidViewModel>> getArtTradeBids(@PathVariable Long artTradeId) {

        List<ArtTradeBidViewModel> artTradeBidViewModels = artTradeService.getById(artTradeId)
                .orElseThrow(() -> new ArtTradeNotFoundException(artTradeId))
                .getArtTradeBid().stream()
                .map(artTradeBidMapper::toDto)
                .collect(Collectors.toList());

        artTradeBidViewModels.remove(0);
        return new ResponseEntity<>(artTradeBidViewModels, HttpStatus.OK);
    }

    @PostMapping("/art-trade/create")
    public ResponseEntity<ArtTradeViewModel> createArtTrade(@Valid @RequestBody ArtTradeViewModel artTradeViewModel,
                                                            BindingResult bindingResult,
                                                            @RequestHeader("UserId") Long userId) {

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        ArtTrade artTrade = artTradeService.createNewArtTrade(artTradeViewModel, userId);

        ArtTradeViewModel newArtTradeModelView = artTradeMapper.toDto(artTrade);
        return new ResponseEntity<>(newArtTradeModelView, HttpStatus.OK);
    }

    @PostMapping("/art-trades/{artTradeId}/art-trade-bids")
    public ResponseEntity<ArtTradeBidViewModel> createArtTradeBid(@Valid @RequestBody ArtTradeBidViewModel artTradeBidViewModel,
                                                                  @PathVariable Long artTradeId,
                                                                  @RequestHeader("UserId") Long userId) {

        ArtTrade artTrade = artTradeService.getById(artTradeId)
                .orElseThrow(() -> new ArtTradeNotFoundException(artTradeId));

        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));
        ArtTradeBid artTradeBid = artTradeService.createNewArtTradeBid(artTrade, user, artTradeBidViewModel);
        ArtTradeBidViewModel artTradeBidViewModelResponse = artTradeBidMapper.toDto(artTradeBid);

        return new ResponseEntity<>(artTradeBidViewModelResponse, HttpStatus.OK);
    }
}
