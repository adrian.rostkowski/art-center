package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.mapper.AuctionAngularMapper;
import com.artcenter.artcenter.mapper.SingleAuctionMapper;
import com.artcenter.artcenter.mapper.UserBidMapper;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.AuctionDTO;
import com.artcenter.artcenter.model.dto.UserBidDTO;
import com.artcenter.artcenter.model.dto.view_model.AuctionAngularDTO;
import com.artcenter.artcenter.model.dto.view_model.MyAccountTransferBox;
import com.artcenter.artcenter.model.dto.view_model.SingleAuctionViewModel;
import com.artcenter.artcenter.service.AuctionService;
import com.artcenter.artcenter.service.BindingResultService;
import com.artcenter.artcenter.service.TransferBoxService;
import com.artcenter.artcenter.service.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j
@RestController
@CrossOrigin(origins = "*")
public class AuctionController {

    private final AuctionService auctionService;
    private final UserBidMapper userBidMapper;
    private final TransferBoxService transferBoxService;
    private final AuctionAngularMapper auctionAngularMapper;
    private final SingleAuctionMapper singleAuctionMapper;
    private final BindingResultService bindingResultService;
    private final UserService userService;

    public AuctionController(AuctionService auctionService,
                             UserBidMapper userBidMapper,
                             TransferBoxService transferBoxService,
                             AuctionAngularMapper auctionAngularMapper,
                             SingleAuctionMapper singleAuctionMapper,
                             BindingResultService bindingResultService,
                             UserService userService) {

        this.auctionService = Objects.requireNonNull(auctionService,
                "auctionService must be not null");
        this.userBidMapper = Objects.requireNonNull(userBidMapper,
                "userBidMapper must be not null");
        this.transferBoxService = Objects.requireNonNull(transferBoxService,
                "transferBoxService must be not null");
        this.auctionAngularMapper = Objects.requireNonNull(auctionAngularMapper,
                "auctionAngularMapper must be not null");
        this.singleAuctionMapper = Objects.requireNonNull(singleAuctionMapper,
                "singleAuctionMapper must be not null");
        this.bindingResultService = Objects.requireNonNull(bindingResultService,
                "bindingResultService must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
    }

    @GetMapping("/user-auction-list")
    public ResponseEntity<List<MyAccountTransferBox>> userAuctionList(@RequestHeader("UserId") String userIdHeader) {
        Long userId = Long.parseLong(userIdHeader);

        List<Auction> auctions = auctionService.findByOwnerId(userId)
                .orElseThrow(() -> new AuctionNotFoundException(userId));
        List<MyAccountTransferBox> auctionDTOS = auctions.stream()
                .filter(Auction::isActive)
                .map(transferBoxService::auctionToMyAccountTransferBox)
                .collect(Collectors.toList());

        return new ResponseEntity<>(auctionDTOS, HttpStatus.OK);
    }

    @GetMapping("/user-auction-list-history")
    public ResponseEntity<List<MyAccountTransferBox>> userAuctionListHistory
            (@RequestHeader("UserId") String userIdHeader) {

        Long userId = Long.parseLong(userIdHeader);

        List<Auction> auctions = auctionService
                .returnAllByUserIdAndIsActive(userId, false)
                .orElseThrow(() -> new AuctionNotFoundException("isActive", false));

        List<MyAccountTransferBox> auctionDTOS = auctions.stream()
                .map(transferBoxService::auctionToMyAccountTransferBox)
                .collect(Collectors.toList());

            return new ResponseEntity<>(auctionDTOS, HttpStatus.OK);
    }

    @GetMapping("/auction/by-id/{id}")
    public ResponseEntity<SingleAuctionViewModel> findAuctionById(@PathVariable String id) {

        Long longId = Long.parseLong(id);
        Auction auctionFound = auctionService.findById(longId)
                .orElseThrow(() -> new AuctionNotFoundException(longId));

        SingleAuctionViewModel singleAuctionViewModel = singleAuctionMapper.toDto(auctionFound);
        return new ResponseEntity<>(singleAuctionViewModel, HttpStatus.OK);
    }

    //todo separate logic between ego and art trade
    @GetMapping("/all-auctions/{mainTypeOfAuction}/{category}/{type}")
    public List<AuctionAngularDTO> filterAuctionsByType(@PathVariable String mainTypeOfAuction,
                                                        @PathVariable String category,
                                                        @PathVariable String type) {

        return findAuctionsByTypeAndCategoryAngular(category, type);
    }

    @GetMapping("/all-auctions")
    public ResponseEntity<List<AuctionAngularDTO>> allAuctionList() {

        List<Auction> auctions = auctionService.returnAllAuctions();
        List<AuctionAngularDTO> auctionDTOS = auctions.stream()
                .filter(Auction::isActive)
                .map(auctionAngularMapper::toDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(auctionDTOS, HttpStatus.OK);
    }

    @GetMapping("/get-auctions-by-artistName/{artistName}")
    public ResponseEntity<List<AuctionAngularDTO>> getAllAuctionsByArtistName(@PathVariable String artistName) {

        Long artistId = userService
                .findByLogin(artistName)
                .orElseThrow(() -> new UserNotFoundException("login", artistName))
                .getId();

        List<Auction> auctions = auctionService.returnAllByUserIdAndIsActive(artistId, true)
                .orElseThrow(() -> new AuctionNotFoundException("isActive", false));

        List<AuctionAngularDTO> auctionDTOS = auctions.stream()
                .filter(Auction::isActive)
                .map(auctionAngularMapper::toDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(auctionDTOS, HttpStatus.OK);
    }

    @PostMapping("/upload-auction-model")
    public ResponseEntity<String> uploadAuctionModel(@Valid @RequestBody AuctionDTO auctionDTO,
                                                     @RequestHeader("UserId") Long userId,
                                                     BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResultService.logBindingResultErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        Long auctionId = auctionService.createNewAuctionAngularVersion(auctionDTO, userId).getId();
        String auctionIdToString = String.valueOf(auctionId);
        return new ResponseEntity<>(auctionIdToString, HttpStatus.OK);
    }

    @PostMapping("/add-new-bid")
    public ResponseEntity<UserBidDTO> addNewBid(@Valid @RequestBody UserBidDTO userBidDTO,
                                                BindingResult bindingResult,
                                                Principal principal) {

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(userBidDTO, HttpStatus.NOT_ACCEPTABLE);
        }

        Auction auction = auctionService
                .findById(Long.parseLong(userBidDTO.getAuctionId()))
                .orElseThrow(() -> new AuctionNotFoundException(userBidDTO.getAuctionId()));

        userBidDTO.setBidOwner(principal.getName());

        UserBid newUserBid = userBidMapper.toEntity(userBidDTO);
        auctionService.addBidToAuctionFromDTO(auction, newUserBid);

        return new ResponseEntity<>(userBidDTO, HttpStatus.OK);
    }

    private List<AuctionAngularDTO> findAuctionsByTypeAndCategoryAngular(String type,
                                                                         String category) {

        List<Auction> auctions;

        if (type.equals("all") && category.equals("all")) {
            auctions = auctionService.findAllByActive(true)
                    .orElseThrow(() -> new AuctionNotFoundException("isActive", true));
        } else if (type.equals("all")) {
            auctions = auctionService.findByTypeOfImage(category)
                    .orElseThrow(() -> new AuctionNotFoundException(category));
        } else if (category.equals("all")) {
            auctions = auctionService.findByTypeOfCharacter(type)
                    .orElseThrow(() -> new AuctionNotFoundException(type));
        } else {
            auctions = auctionService
                    .findByTypeOfCharacterAndTypeOfImage(type, category)
                    .orElseThrow(() -> new AuctionNotFoundException(type, category));
        }

        return auctions.stream()
                .filter(Auction::isActive)
                .map(auctionAngularMapper::toDto)
                .collect(Collectors.toList());
    }
}
