package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.UserDataToSetHeaders;
import com.artcenter.artcenter.model.dto.UserDTO;
import com.artcenter.artcenter.service.BindingResultService;
import com.artcenter.artcenter.service.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Objects;

@Log4j
@RestController
@CrossOrigin(origins = "*")
public class UserController {

    private final UserService userService;
    private final BindingResultService bindingResultService;

    public UserController(UserService userService, BindingResultService bindingResultService) {
        this.userService = Objects.requireNonNull(userService, "userService must be not null");
        this.bindingResultService = Objects.requireNonNull(bindingResultService, "bindingResultService must be not null");
    }


    @GetMapping("/login")
    public ResponseEntity<UserDataToSetHeaders> login(Principal principal) {
        String userLogin = principal.getName();
        User user = userService.findByLogin(userLogin).orElseThrow(() -> new UserNotFoundException("login", userLogin));

        UserDataToSetHeaders userDataToSetHeaders = UserDataToSetHeaders.builder()
                .userId(user.getId().toString())
                .userAvatarId(findUserAvatarImageId(user).toString())
                .build();

        return new ResponseEntity<>(userDataToSetHeaders, HttpStatus.OK);
    }

    private Long findUserAvatarImageId(User user) {
        if (user.getUserAvatar() == null) {
            return 0L;
        } else {
            return user.getUserAvatar().getId();
        }
    }

    @PostMapping("/create-new-user")
    public ResponseEntity<UserDTO> createNewUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResultService.throwBindingResultValidException(bindingResult);
        }

        User user = userService.makeNewUser(userDTO);
        UserDTO authenticationCodeAndLogin = UserDTO.builder()
                .authenticationCode(user.getAuthenticationCode())
                .login(user.getLogin())
                .build();

        return new ResponseEntity<>(authenticationCodeAndLogin, HttpStatus.OK);
    }

    @PostMapping("/create-new-user-authentication")
    public ResponseEntity<Void> activeAccount(@RequestBody UserDTO userDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResultService.logBindingResultErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        if (userService.checkAuthenticationCode(userDTO.getAuthenticationCode(), userDTO.getAuthenticationCodeInput())) {
            userService.activeUser(userDTO.getLogin());
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
