package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.NewFriendDTO;
import com.artcenter.artcenter.service.FriendService;
import com.artcenter.artcenter.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Objects;

@Controller
public class UserFriendsController {

    private final UserService userService;
    private final FriendService friendService;

    public UserFriendsController(UserService userService, FriendService friendService) {
        this.userService = Objects.requireNonNull(userService, "userService must be not null");
        this.friendService = Objects.requireNonNull(friendService, "friendService must be not null");
    }

    @GetMapping("/friend-list")
    public ModelAndView friendList(Principal principal) {
        User user = userService.findByLogin(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("login", principal.getName()));

        return new ModelAndView("friend-list")
                .addObject("userFriendsSet", user.getFriendsList())
                .addObject("userFriendOfSet", user.getFriendOf())
                .addObject("userInvitesSet", user.getInvitesToFriendListSet());
    }

    @GetMapping("/create-new-friend")
    public ModelAndView returnCreateFriendForm() {
        return new ModelAndView("add-to-invite-list")
                .addObject("newFriendDTO", NewFriendDTO.builder().build());
    }

    @GetMapping("/delete-friend")
    public ModelAndView deleteFriend(Principal principal,
                                     @RequestParam Long id) {

        User user = userService.findByLogin(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("login", principal.getName()));

        friendService.deleteFriend(user.getId(), id);

        return new ModelAndView("friend-list")
                .addObject("userFriendsSet", user.getFriendsList())
                .addObject("userInvitesSet", user.getInvitesToFriendListSet());
    }
}
