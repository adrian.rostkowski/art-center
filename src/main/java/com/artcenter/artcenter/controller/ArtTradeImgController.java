package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.ArtTradeImgNotFoundException;
import com.artcenter.artcenter.model.ArtTradeImg;
import com.artcenter.artcenter.service.ArtTradeImgService;
import lombok.extern.log4j.Log4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Log4j
@RestController
@CrossOrigin(origins = "*")
public class ArtTradeImgController {

    private final ArtTradeImgService artTradeImgService;

    public ArtTradeImgController(ArtTradeImgService artTradeImgService) {
        this.artTradeImgService = Objects.requireNonNull(artTradeImgService,
                "artTradeImgService must be not null");
    }

    @GetMapping("/art-trade-image/download/{imageId}")
    public ResponseEntity<Resource> getUserAvatar(@PathVariable Long imageId) {
        ArtTradeImg artTradeImg = artTradeImgService.getById(imageId)
                .orElseThrow(() -> new ArtTradeImgNotFoundException(imageId));

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(artTradeImg.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + artTradeImg.getFileName() + "\"")
                .body(new ByteArrayResource(artTradeImg.getData()));
    }

    @PostMapping("/art-trade-img/save")
    public ResponseEntity<Long> save(@RequestParam("file") MultipartFile file,
                                     @RequestParam("category") String category,
                                     @RequestHeader("UserId") Long userIdHeader) {
        if (file.isEmpty()) {
            log.debug("Request Param in post /art-trade-img/save can not be empty");
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Long imageId = artTradeImgService.storeFile(file, userIdHeader, category).getId();
        return new ResponseEntity<>(imageId, HttpStatus.OK);
    }
}
