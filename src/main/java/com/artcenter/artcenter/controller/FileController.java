package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.FileNotFoundException;
import com.artcenter.artcenter.exception.UserAvatarNotFoundException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.Img;
import com.artcenter.artcenter.model.UserAvatar;
import com.artcenter.artcenter.playload.UploadFileResponse;
import com.artcenter.artcenter.service.ImgService;
import com.artcenter.artcenter.service.UserAvatarService;
import com.artcenter.artcenter.service.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j
@RestController
@CrossOrigin(origins = "*")
public class FileController {

    private final ImgService imgService;
    private final UserService userService;
    private final UserAvatarService userAvatarService;

    public FileController(ImgService imgService, UserService userService, UserAvatarService userAvatarService) {
        this.imgService = Objects.requireNonNull(imgService, "imgService must be not null");
        this.userService = Objects.requireNonNull(userService, "userService must be not null");
        this.userAvatarService = Objects.requireNonNull(userAvatarService, "userAvatarService must be not null");
    }

    @GetMapping("/downloadFile/{imageId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String imageId) {
        Img img = imgService.getFile(imageId)
                .orElseThrow(() -> new FileNotFoundException("File not found with id " + imageId));

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(img.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + img.getFileName() + "\"")
                .body(new ByteArrayResource(img.getData()));
    }

    @GetMapping("/get-user-avatar/{imageId}")
    public ResponseEntity<Resource> getUserAvatar(@PathVariable Long imageId) {
        UserAvatar userAvatar = userAvatarService.getUserAvatar(imageId)
                .orElseThrow(() -> new UserAvatarNotFoundException(imageId));

        return ResponseEntity.ok()
                .body(new ByteArrayResource(userAvatar.getData()));
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("category") String category,
                                                        @RequestParam("files") MultipartFile[] files,
                                                        @RequestHeader("UserId") Long userIdHeader) {

        return Arrays.stream(files)
                .map(file -> uploadFile(category, file, userIdHeader))
                .collect(Collectors.toList());
    }

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("category") String category,
                                         @RequestParam("file") MultipartFile file,
                                         @RequestHeader("UserId") Long userIdHeader) {

        Long userId = userService
                .findById(userIdHeader)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userIdHeader)))
                .getId();

        Img img = imgService.storeFile(file, userId, category);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(img.getId())
                .toUriString();

        return new UploadFileResponse(img.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @PostMapping("/upload-auction-image")
    public ResponseEntity<String> saveAuctionImg(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("category") String category,
                                                 @RequestHeader("UserId") Long userIdHeader) {

        if (file.isEmpty()) {
            log.debug("Request Param in post /upload-auction-image can not be empty");
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Long userId = userService
                .findById(userIdHeader)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userIdHeader)))
                .getId();

        String imageId = imgService.storeFile(file, userId, category).getId();
        return new ResponseEntity<>(imageId, HttpStatus.OK);
    }

    @PostMapping("/upload-user-avatar-image")
    public ResponseEntity<Long> saveUserAvatar(@RequestParam("file") MultipartFile file,
                                               @RequestHeader("UserId") Long userIdHeader) {

        if (file.isEmpty()) {
            log.debug("Request Param in post /upload-auction-image can not be empty");
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Long userAvatarId = imgService.storeUserAvatar(file, userIdHeader).getId();

        return new ResponseEntity<>(userAvatarId, HttpStatus.OK);
    }
}
