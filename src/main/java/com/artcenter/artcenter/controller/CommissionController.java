package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.CommissionNotFoundException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.mapper.CommissionCommentMapper;
import com.artcenter.artcenter.mapper.CommissionMapper;
import com.artcenter.artcenter.model.Comment;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.view_model.CommissionCommentViewModel;
import com.artcenter.artcenter.model.dto.view_model.CommissionViewModel;
import com.artcenter.artcenter.service.CommentService;
import com.artcenter.artcenter.service.CommissionService;
import com.artcenter.artcenter.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@Controller
@CrossOrigin(origins = "*")
public class CommissionController {

    private final UserService userService;
    private final CommissionService commissionService;
    private final CommentService commentService;
    private final CommissionMapper commissionMapper;
    private final CommissionCommentMapper commissionCommentMapper;

    public CommissionController(UserService userService, CommissionService commissionService, CommentService commentService, CommissionMapper commissionMapper, CommissionCommentMapper commissionCommentMapper) {
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.commissionService = Objects.requireNonNull(commissionService,
                "commissionService must be not null");
        this.commentService = Objects.requireNonNull(commentService,
                "commentService must be not null");
        this.commissionMapper = Objects.requireNonNull(commissionMapper,
                "commissionMapper must be not null");
        this.commissionCommentMapper = Objects.requireNonNull(commissionCommentMapper,
                "commissionCommentMapper must be not null");
    }

    @GetMapping("/commission-angular/{id}")
    public ResponseEntity<CommissionViewModel> findCommissionByIdAngular(@PathVariable Long id,
                                                                         Principal principal) {

        Commission commission = commissionService.findById(id)
                .orElseThrow(() -> new CommissionNotFoundException(id));

        String userName = principal.getName();

        if (userName.equals(commission.getArtist()) || userName.equals(commission.getWinner())) {
            CommissionViewModel commissionViewModel = commissionMapper.toDto(commission);
            return new ResponseEntity<>(commissionViewModel, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Transactional
    @PostMapping("/commission-post-comment/{commissionId}")
    public ResponseEntity<CommissionCommentViewModel> addCommentAngular(
            @PathVariable Long commissionId,
            @RequestBody CommissionCommentViewModel commissionCommentViewModel,
            @RequestHeader("UserId") Long userId) {

        Commission commission = commissionService.findById(commissionId)
                .orElseThrow(() -> new CommissionNotFoundException(commissionId));

        User commentOwner = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        Comment newComment = commentService.createNewComment(
                commissionCommentViewModel.getText(),
                commission,
                commentOwner);

        Comment commentSaved = commentService.save(newComment);

        commentService.addNewCommentToCommission(commission, commentSaved);

        CommissionCommentViewModel newCommissionCommentViewModel = commissionCommentMapper.toDto(commentSaved);

        return new ResponseEntity<>(newCommissionCommentViewModel, HttpStatus.OK);
    }
}
