package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.CommissionNotFoundException;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.dto.view_model.MyAccountSetUpViewModel;
import com.artcenter.artcenter.model.dto.view_model.MyAccountTransferBox;
import com.artcenter.artcenter.service.CommissionService;
import com.artcenter.artcenter.service.TransferBoxService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@CrossOrigin(origins = "*")
public class MessagesController {

    private final CommissionService commissionService;
    private final TransferBoxService transferBoxService;

    public MessagesController(CommissionService commissionService,
                              TransferBoxService transferBoxService) {

        this.commissionService = Objects.requireNonNull(commissionService,
                "commissionService must be not null");
        this.transferBoxService = Objects.requireNonNull(transferBoxService,
                "transferBoxService must be not null");
    }

    @GetMapping("/messages")
    public ResponseEntity<MyAccountSetUpViewModel> getMessages(Principal principal) {

        String userName = principal.getName();

        long countOfCommissions = commissionService
                .findByArtistName(userName)
                .orElseThrow(() -> new CommissionNotFoundException("artistName"))
                .size();

        long countOfWonAuctions = commissionService
                .findByWinnerName(userName)
                .orElseThrow(() -> new CommissionNotFoundException("artistName"))
                .size();

        MyAccountSetUpViewModel myAccountSetUpViewModel = MyAccountSetUpViewModel.builder()
                .countOfCommissions(countOfCommissions)
                .countOfWonAuctions(countOfWonAuctions)
                .build();

        return new ResponseEntity<>(myAccountSetUpViewModel, HttpStatus.OK);
    }

    @GetMapping("/messages/list/commission")
    public ResponseEntity<List<MyAccountTransferBox>> getCommissionsAngular(Principal principal) {

        List<Commission> commissionList = commissionService
                .findByArtistName(principal.getName())
                .orElseThrow(() -> new CommissionNotFoundException("artistName"));
        List<MyAccountTransferBox> myAccountTransferBoxes = commissionList.stream()
                .map(transferBoxService::commissionToMyAccountTransferBox)
                .collect(Collectors.toList());

        return new ResponseEntity<>(myAccountTransferBoxes, HttpStatus.OK);
    }

    @GetMapping("/messages/list/wonAuctions")
    public ResponseEntity<List<MyAccountTransferBox>> getWonAuctionsAngular(Principal principal) {

        List<Commission> commissionList = commissionService
                .findByWinnerName(principal.getName())
                .orElseThrow(() -> new CommissionNotFoundException("artistName"))
                .stream()
                .filter(Commission::isActive)
                .collect(Collectors.toList());

        List<MyAccountTransferBox> myAccountTransferBoxes = commissionList.stream()
                .map(transferBoxService::wonAuctionsToMyAccountTransferBox)
                .collect(Collectors.toList());

        return new ResponseEntity<>(myAccountTransferBoxes, HttpStatus.OK);
    }

    @GetMapping("/messages/list/auctions/ended")
    public ResponseEntity<List<MyAccountTransferBox>> getAllEndedAuctions(Principal principal) {
        List<Commission> commissionList = commissionService
                .findByWinnerName(principal.getName())
                .orElseThrow(() -> new CommissionNotFoundException("artistName"))
                .stream()
                .filter(commission -> !commission.isActive())
                .collect(Collectors.toList());

        List<MyAccountTransferBox> myAccountTransferBoxes = commissionList.stream()
                .map(transferBoxService::wonAuctionsToMyAccountTransferBox)
                .collect(Collectors.toList());

        return new ResponseEntity<>(myAccountTransferBoxes, HttpStatus.OK);
    }
}
