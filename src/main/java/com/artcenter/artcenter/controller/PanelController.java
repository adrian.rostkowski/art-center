package com.artcenter.artcenter.controller;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.mapper.ArtistPanelMapper;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.view_model.ArtistPanelViewModel;
import com.artcenter.artcenter.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Objects;

@CrossOrigin(origins = "*")
@Controller
public class PanelController {

    private final UserService userService;
    private final ArtistPanelMapper artistPanelMapper;

    public PanelController(UserService userService, ArtistPanelMapper artistPanelMapper) {
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.artistPanelMapper = Objects.requireNonNull(artistPanelMapper,
                "artistPanelMapper must be not null");
    }

    @GetMapping("/artist-panel/{artistName}")
    public ResponseEntity<ArtistPanelViewModel> getArtistPanel(@PathVariable String artistName) {
        User artist = userService
                .findByLogin(artistName)
                .orElseThrow(() -> new UserNotFoundException("login", artistName));
        ArtistPanelViewModel artistPanelViewModel = artistPanelMapper.toDto(artist);

        return new ResponseEntity<>(artistPanelViewModel, HttpStatus.OK);
    }
}
