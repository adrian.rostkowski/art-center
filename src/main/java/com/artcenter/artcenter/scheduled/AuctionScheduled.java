package com.artcenter.artcenter.scheduled;

import com.artcenter.artcenter.exception.AuctionNotFoundException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.service.AuctionService;
import com.artcenter.artcenter.service.CommissionService;
import com.artcenter.artcenter.service.NotificationService;
import com.artcenter.artcenter.service.UserService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AuctionScheduled {

    //TODO load this from application.properties
    private static final String NAME_FOR_STARTING_PRICE = "Starting Price";
    private final AuctionService auctionService;
    private final NotificationService notificationService;
    private final UserService userService;
    private final CommissionService commissionService;

    public AuctionScheduled(AuctionService auctionService, NotificationService notificationService, UserService userService, CommissionService commissionService) {
        this.auctionService = Objects.requireNonNull(auctionService,
                "auctionService must be not null");
        this.notificationService = Objects.requireNonNull(notificationService,
                "notificationService must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.commissionService = Objects.requireNonNull(commissionService,
                "commissionService must be not null");
    }

    //TODO add some optimization, right now we search in all active auctions
    @Scheduled(fixedRate = 10000)
    @Transactional
    public void scheduleFixedDelayTask() {
        deactivateAuctions();
    }

    private void deactivateAuctions() {
        List<Auction> activeAuctionsList = auctionService
                .findAllByActiveOrAuctionOverByAutoBuy(true, true)
                .orElseThrow(() -> new AuctionNotFoundException("isActive", true, "isAuctionOverByAutoBuy", true));

        List<Auction> deactivatedAuctions = activeAuctionsList.stream()
                .filter(auction -> auction.getDebtEndDate().isBefore(LocalDateTime.now()) || auction.isAuctionOverByAutoBuy())
                .collect(Collectors.toList());

        deactivatedAuctions
                .forEach(auction -> {
                    auction.setActive(false);
                    auction.setAuctionOverByAutoBuy(false);
                    sentNotificationIfWasAWinner(auction);
                    auctionService.save(auction);
                });
    }

    private Long makeCommission(Auction auction) {
        Commission commission = commissionService.makeNewCommission(auction);
        return commission.getId();
    }

    private void sentNotificationIfWasAWinner(Auction auction) {
        List<UserBid> userBidList = auction.getUserBids();
        String winnerName = userBidList.get(userBidList.size() - 1).getBidOwner();

        if (winnerName.equals(NAME_FOR_STARTING_PRICE)) {
            return;
        }

        User winner = userService.findByLogin(winnerName)
                .orElseThrow(() -> new UserNotFoundException("login", winnerName));
        User artist = userService.findByLogin(auction.getOwnerName())
                .orElseThrow(() -> new UserNotFoundException("login", auction.getOwnerName()));
        Long commissionId = makeCommission(auction);

        String subjectToWinner = "You are a winner!";
        String subjectToArtist = "Your auction is over have a winner!";

        String text = "Auction name : " + auction.getTitle() + "\n localhost:8080/commission/" + commissionId;

        notificationService.sendNotificationToWinner(winner.getEmail(), subjectToWinner, text);
        notificationService.sendNotificationToWinner(artist.getEmail(), subjectToArtist, text);
    }
}
