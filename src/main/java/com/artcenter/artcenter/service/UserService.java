package com.artcenter.artcenter.service;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.Role;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.UserDTO;
import com.artcenter.artcenter.repositories.UserRepository;
import io.vavr.concurrent.Future;
import lombok.extern.log4j.Log4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;

@Log4j
@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final NotificationService notificationService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final FriendListTokenService friendListTokenService;

    public UserService(RoleService roleService, NotificationService notificationService,
                       BCryptPasswordEncoder passwordEncoder, UserRepository userRepository,
                       FriendListTokenService friendListTokenService) {

        this.roleService = Objects.requireNonNull(roleService,
                "roleService must be not null");
        this.userRepository = Objects.requireNonNull(userRepository,
                "userRepository must be not null");
        this.notificationService = Objects.requireNonNull(notificationService,
                "notificationService must be not null");
        this.passwordEncoder = Objects.requireNonNull(passwordEncoder,
                "passwordEncoder must be not null");
        this.friendListTokenService = Objects.requireNonNull(friendListTokenService,
                "friendListTokenService must be not null");
    }

    public Optional<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public User makeNewUser(UserDTO userDTO) {
        String authenticationCode = generateNewAuthenticationCode();

        //todo load ROLE_USER from enum
        Role userRole = roleService.findByName("ROLE_USER")
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Unable to get Role name : [%s]", "ROLE_USER")));

        User user = User.builder()
                .login(userDTO.getLogin())
                .password(passwordEncoder.encode(userDTO.getPassword1()))
                .roles(new HashSet<>(Arrays.asList(userRole)))
                .email(userDTO.getEmail())
                .authenticationCode(authenticationCode)
                .build();

        Future.of(() -> notificationService.sendActiveCodeNotification(user.getEmail(), authenticationCode));

        save(user);
        friendListTokenService.createFriendListToken(user);
        return user;
    }

    private String generateNewAuthenticationCode() {
        String authenticationCode = String.valueOf((int) (Math.random() * 1000000));
        log.debug("Authentication code : " + authenticationCode);
        return authenticationCode;
    }

    public boolean checkAuthenticationCode(String authCode, String authCodeInput) {
        return authCode.equals(authCodeInput);
    }

    public boolean isThisUserNameExist(String userName) {
        Optional<User> found = userRepository.findByLogin(userName);
        return found.isPresent();
    }

    public void activeUser(String login) {
        User user = findByLogin(login)
                .orElseThrow(() -> new UserNotFoundException("login", login));

        user.setActive(1);
        save(user);
    }
}
