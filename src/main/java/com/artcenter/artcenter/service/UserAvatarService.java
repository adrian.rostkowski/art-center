package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.UserAvatar;
import com.artcenter.artcenter.repositories.UserAvatarRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserAvatarService {

    private final UserAvatarRepository userAvatarRepository;

    public UserAvatarService(UserAvatarRepository userAvatarRepository) {
        this.userAvatarRepository = Objects.requireNonNull(userAvatarRepository,
                "userAvatarRepository must be not null");
    }

    public UserAvatar saveUserAvatar(UserAvatar userAvatarToSave) {
        return userAvatarRepository.save(userAvatarToSave);
    }

    public Optional<UserAvatar> getUserAvatar(Long imageId) {
        return userAvatarRepository.findById(imageId);
    }
}
