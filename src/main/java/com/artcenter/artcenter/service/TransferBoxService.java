package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.dto.view_model.MyAccountTransferBox;
import org.springframework.stereotype.Service;

@Service
public class TransferBoxService {

    public MyAccountTransferBox auctionToMyAccountTransferBox(Auction auction) {
        return MyAccountTransferBox.builder()
                .transferObjectId(auction.getId())
                .imageId(auction.getImageId())
                .build();
    }

    public MyAccountTransferBox commissionToMyAccountTransferBox(Commission commission) {
        return MyAccountTransferBox.builder()
                .transferObjectId(commission.getId())
                .imageId(commission.getAuction().getImageId())
                .build();
    }

    public MyAccountTransferBox wonAuctionsToMyAccountTransferBox(Commission commission) {
        return MyAccountTransferBox.builder()
                .transferObjectId(commission.getId())
                .imageId(commission.getAuction().getImageId())
                .build();
    }

    public MyAccountTransferBox galleryImgIdToMyAccountTransferBox(String galleryImageId) {
        return MyAccountTransferBox.builder()
                .transferObjectId(Long.parseLong(galleryImageId))
                .imageId(Long.parseLong(galleryImageId))
                .build();
    }
}
