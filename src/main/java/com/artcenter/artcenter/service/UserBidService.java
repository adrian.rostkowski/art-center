package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.repositories.UserBidRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

@Service
public class UserBidService {
    private final UserBidRepository userBidRepository;

    public UserBidService(UserBidRepository userBidRepository) {
        this.userBidRepository = Objects.requireNonNull(userBidRepository, "userBidRepository must be not null");
    }

    public UserBid save(UserBid newUserBid) {
        return userBidRepository.save(newUserBid);
    }

    public int compareToTheHighest(List<UserBid> bids, BigDecimal bid) {
        TreeSet<UserBid> treeSet = new TreeSet<>(bids);
        if (treeSet.size() == 0) {
            return 1;
        }
        return bid.compareTo(treeSet.first().getBidValue());
    }

    public BigDecimal returnTheHighestBid(Auction auction) {
        TreeSet<UserBid> userBidTreeSet = new TreeSet<>(auction.getUserBids());
        return userBidTreeSet.first().getBidValue();

    }

    public UserBid makeInitialUserBid(Auction auction) {
        return UserBid.builder()
                .bidOwner("Starting Price")
                .bidValue(auction.getStartingBid())
                .auction(auction)
                .build();
    }
}
