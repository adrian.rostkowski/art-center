package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.FriendListToken;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.repositories.FriendListTokenRepo;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Log4j
@Service
public class FriendListTokenService {

    private final FriendListTokenRepo friendListTokenRepo;

    public FriendListTokenService(FriendListTokenRepo friendListTokenRepo) {
        this.friendListTokenRepo = Objects.requireNonNull(friendListTokenRepo,
                "friendListTokenRepo must be not null");
    }

    public FriendListToken createFriendListToken(User user) {
        FriendListToken invitesToFriendList = FriendListToken.builder()
                .userName(user.getLogin())
                .userId(user.getId())
                .build();

        return save(invitesToFriendList);
    }

    public Optional<FriendListToken> findByUserName(String name) {
        return friendListTokenRepo.findByUserName(name);
    }

    public Optional<FriendListToken> findByUserId(Long id) {
        return friendListTokenRepo.findByUserId(id);
    }

    public FriendListToken save(FriendListToken friendListToken) {
        return friendListTokenRepo.save(friendListToken);
    }
}
