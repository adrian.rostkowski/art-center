package com.artcenter.artcenter.service;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.ArtTrade;
import com.artcenter.artcenter.model.ArtTradeBid;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeBidViewModel;
import com.artcenter.artcenter.model.dto.view_model.ArtTradeViewModel;
import com.artcenter.artcenter.repositories.ArtTradeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ArtTradeService {
    private final ArtTradeRepository artTradeRepository;
    private final UserService userService;
    private final ArtTradeBidService artTradeBidService;

    public ArtTradeService(ArtTradeRepository artTradeRepository, UserService userService, ArtTradeBidService artTradeBidService) {
        this.artTradeRepository = Objects.requireNonNull(artTradeRepository,
                "artTradeRepository must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.artTradeBidService = Objects.requireNonNull(artTradeBidService,
                "artTradeBidService must be not null");
    }

    public ArtTrade save(ArtTrade artTrade) {
        return artTradeRepository.save(artTrade);
    }

    public Optional<ArtTrade> getById(Long id) {
        return artTradeRepository.findById(id);
    }

    public Optional<List<ArtTrade>> getByIsActive(boolean isActive) {
        return artTradeRepository.findAllByIsActive(isActive);
    }

    @Transactional
    public ArtTrade createNewArtTrade(ArtTradeViewModel artTradeViewModel, Long ownerId) {
        User owner = userService.findById(ownerId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(ownerId)));

        ArtTrade artTrade = ArtTrade.builder()
                .endDateOfArtTrade(returnAuctionEndDate(artTradeViewModel.getCountOfDays()))
                .description(artTradeViewModel.getDescription())
                .imageId(artTradeViewModel.getImageId())
                .title(artTradeViewModel.getTitle())
                .owner(owner)
                .isActive(true)
                .artTradeBid(new ArrayList<>())
                .typeOfCharacter(artTradeViewModel.getTypeOfCharacter())
                .categoryOfImage(artTradeViewModel.getCategoryImg())
                .build();

        save(artTrade);

        ArtTradeBid artTradeBid = artTradeBidService.createInitialArtTradeBid(artTrade, owner);
        artTradeBidService.save(artTradeBid);

        artTrade.getArtTradeBid().add(artTradeBid);

        ArtTrade savedArtTrade = save(artTrade);
        addNewArtTradeToOwner(savedArtTrade, owner);
        return savedArtTrade;
    }

    public ArtTradeBid createNewArtTradeBid(ArtTrade artTrade, User owner, ArtTradeBidViewModel artTradeBidViewModel) {

        ArtTradeBid artTradeBid = ArtTradeBid.builder()
                .artTrade(artTrade)
                .description(artTradeBidViewModel.getDescription())
                .imageId(artTradeBidViewModel.getImageId())
                .owner(owner)
                .build();

        ArtTradeBid artTradeBidSaved = artTradeBidService.save(artTradeBid);
        List<ArtTradeBid> artTradeBids = artTrade.getArtTradeBid();
        artTradeBids.add(artTradeBidSaved);
        save(artTrade);

        return artTradeBidSaved;
    }

    private void addNewArtTradeToOwner(ArtTrade newArtTrade, User owner) {
        owner.getArtTradeList().add(newArtTrade);
        userService.save(owner);
    }

    private LocalDateTime returnAuctionEndDate(Long countOfDays) {
        LocalDateTime localDateTimeNow = LocalDateTime.now();
        return localDateTimeNow.plusDays(countOfDays);
    }
}
