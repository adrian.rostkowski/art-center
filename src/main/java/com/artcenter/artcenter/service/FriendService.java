package com.artcenter.artcenter.service;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.FriendListToken;
import com.artcenter.artcenter.model.User;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;
import java.util.Set;

@Log4j
@Service
public class FriendService {

    private final FriendListTokenService friendListTokenService;
    private final UserService userService;

    public FriendService(FriendListTokenService friendListTokenService, UserService userService) {
        this.friendListTokenService = Objects.requireNonNull(friendListTokenService,
                "friendListTokenService must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
    }

    public void addToInvitedList(Long actualUserId, String newFriendName) {
        User actualUser = userService.findById(actualUserId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(actualUserId)));

        User newFriend = userService.findByLogin(newFriendName)
                .orElseThrow(() -> new UserNotFoundException("login", newFriendName));

        FriendListToken friendListToken = friendListTokenService.findByUserName(actualUser.getLogin())
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Unable to get FriendListToken name : [%s]", actualUser.getLogin())));

        Set<FriendListToken> invitesToFriendList = newFriend.getInvitesToFriendListSet();
        invitesToFriendList.add(friendListToken);
        newFriend.setInvitesToFriendListSet(invitesToFriendList);
        userService.save(newFriend);
    }

    @Transactional
    public void addToFriendList(Long actualUserId, Long newFriendId) {
        User actualUser = userService.findById(actualUserId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(actualUserId)));

        User newFriend = userService.findById(newFriendId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(newFriendId)));

        Set<User> friendList = actualUser.getFriendsList();
        friendList.add(newFriend);
        actualUser.setFriendsList(friendList);
        userService.save(actualUser);

        deleteFromInviteList(actualUser.getId(), newFriend.getId());
    }

    private void deleteFromInviteList(Long userId, Long userInListId) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        FriendListToken invitesToFriendListFrom = friendListTokenService.findByUserId(userInListId)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Unable to get FriendListToken id : [%s]", userInListId)));

        Set<FriendListToken> invitesToFriendList = user.getInvitesToFriendListSet();
        invitesToFriendList.remove(invitesToFriendListFrom);
        user.setInvitesToFriendListSet(invitesToFriendList);

        userService.save(user);
    }

    @Transactional
    public void deleteFriend(Long userId, Long friendToRemoveId) {
        User actualUser = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        User friendToRemove = userService.findById(friendToRemoveId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(friendToRemoveId)));

        Set<User> actualUserFriendList = actualUser.getFriendsList();
        Set<User> friendToRemoveFriendList = friendToRemove.getFriendsList();

        if (!actualUserFriendList.remove(friendToRemove)) {
            log.warn(String.format("Unable to remove [%s] from [%s] friendsList", friendToRemove, actualUser));
        }
        if (!friendToRemoveFriendList.remove(actualUser)) {
            log.warn(String.format("Unable to remove [%s] from [%s] friendsList", actualUser, friendToRemove));
        }

        userService.save(actualUser);
        userService.save(friendToRemove);
    }
}
