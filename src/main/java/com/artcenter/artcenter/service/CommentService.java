package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.Comment;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.repositories.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommissionService commissionService;

    public CommentService(CommentRepository commentRepository, CommissionService commissionService) {
        this.commentRepository = Objects.requireNonNull(commentRepository,
                "commentRepository must be not null");
        this.commissionService = Objects.requireNonNull(commissionService,
                "commissionService must be not null");
    }

    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    public Comment createNewComment(String text, Commission commission, User commentOwner) {
        return Comment.builder()
                .commission(commission)
                .text(text)
                .user(commentOwner)
                .build();
    }

    public void addNewCommentToCommission(Commission commission, Comment comment) {
        List<Comment> commentsList = commission.getComments();
        commentsList.add(comment);
        commission.setComments(commentsList);

        commissionService.save(commission);
    }
}
