package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.ArtTrade;
import com.artcenter.artcenter.model.ArtTradeBid;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.repositories.ArtTradeBidRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ArtTradeBidService {

    private final ArtTradeBidRepository artTradeBidRepository;


    public ArtTradeBidService(ArtTradeBidRepository artTradeBidRepository) {

        this.artTradeBidRepository = Objects.requireNonNull(artTradeBidRepository,
                "artTradeBidRepository must be not null");
    }

    public ArtTradeBid save(ArtTradeBid artTradeBid) {
        return artTradeBidRepository.save(artTradeBid);
    }

    public ArtTradeBid createInitialArtTradeBid(ArtTrade artTrade, User owner) {

        String initialDescription = "";
        Long initialImageId = 0L;
        ArtTradeBid artTradeBid = ArtTradeBid.builder()
                .artTrade(artTrade)
                .description(initialDescription)
                .imageId(initialImageId)
                .owner(owner)
                .build();

        return artTradeBidRepository.save(artTradeBid);
    }
}
