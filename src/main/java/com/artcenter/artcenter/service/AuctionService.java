package com.artcenter.artcenter.service;

import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.Img;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.UserBid;
import com.artcenter.artcenter.model.dto.AuctionDTO;
import com.artcenter.artcenter.repositories.AuctionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class AuctionService {

    //todo this should be enum / load this from property file
    private static final String CATEGORY = "auction";
    private final AuctionRepository auctionRepository;
    private final UserService userService;
    private final ImgService imgService;
    private final UserBidService userBidService;

    public AuctionService(AuctionRepository auctionRepository,
                          UserService userService,
                          ImgService imgService,
                          UserBidService userBidService) {

        this.auctionRepository = Objects.requireNonNull(auctionRepository,
                "auctionRepository must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
        this.imgService = Objects.requireNonNull(imgService,
                "imgService must be not null");
        this.userBidService = Objects.requireNonNull(userBidService,
                "userBidService must be not null");
    }

    private Auction makeNewAuctionObject(AuctionDTO auctionDTO, Long ownerId, String userLogin) {
        LocalDateTime auctionEndDate = returnAuctionEndDate(auctionDTO.getAuctionTime());
        User owner = userService.findById(ownerId)
                .orElseThrow(() -> new UserNotFoundException("id", ownerId.toString()));

        return Auction.builder()
                .title(auctionDTO.getTitle())
                .description(auctionDTO.getDescription())
                .typeOfImage(auctionDTO.getTypeOfImage())
                .typeOfCharacter(auctionDTO.getTypeOfCharacter())
                .imageId(auctionDTO.getImageId())
                .startingBid(auctionDTO.getStartingBid())
                .minimumBid(auctionDTO.getMinimumBid())
                .autoBuy(auctionDTO.getAutoBuy())
                .isActive(true)
                .owner(owner)
                .debtEndDate(auctionEndDate)
                .ownerName(userLogin)
                .userBids(new ArrayList<>())
                .build();
    }

    private Auction saveNewAuction(Auction auction) {
        return auctionRepository.save(auction);
    }

    public Optional<List<Auction>> findByTypeOfCharacter(String type) {
        return auctionRepository.findByTypeOfCharacter(type);
    }

    public Optional<List<Auction>> findByTypeOfImage(String type) {
        return auctionRepository.findByTypeOfImage(type);
    }

    private LocalDateTime returnAuctionEndDate(String countOfDays) {
        LocalDateTime localDateTimeNow = LocalDateTime.now();
        return localDateTimeNow.plusDays(Integer.parseInt(countOfDays));
    }

    public Optional<List<Auction>> findByOwnerId(Long id) {
        return auctionRepository.findAllByOwnerId(id);
    }

    public Optional<List<Auction>> findAllByActive(boolean isActive) {
        return auctionRepository.findAllByIsActive(isActive);
    }

    public Optional<List<Auction>> findAllByActiveOrAuctionOverByAutoBuy(boolean isActive, boolean isAuctionOverByAutoBuy) {
        return auctionRepository.findAllByIsActiveOrIsAuctionOverByAutoBuy(isActive, isAuctionOverByAutoBuy);
    }

    //todo this is the same method like findByOwnerId(Long id)
    public Optional<List<Auction>> returnAllUserAuctions(Long ownerId) {
        return auctionRepository.findAllByOwnerId(ownerId);
    }

    public Optional<List<Auction>> returnAllByUserIdAndIsActive(Long ownerId, boolean isActive) {
        return auctionRepository.findAllByOwnerIdAndIsActive(ownerId, isActive);
    }

    public Optional<Auction> findById(Long id) {
        return auctionRepository.findById(id);
    }

    @Transactional
    public void addBidToAuctionFromDTO(Auction auction, UserBid newUserBid) {
        boolean isAuctionValid = auction.getDebtEndDate().isAfter(LocalDateTime.now());
        if (!isAuctionValid) {
            return;
        }
        if (isThisBidLowerThenAutoBuy(newUserBid.getBidValue(), auction.getAutoBuy())) {
            List<UserBid> userBidsUpdated = auction.getUserBids();
            userBidsUpdated.add(newUserBid);
            userBidService.save(newUserBid);
            save(auction);
        } else {
            endAuctionByAutoBuy(auction, newUserBid);
        }
    }

    @Transactional
    public Auction createNewAuction(AuctionDTO auctionDTO, Long userId) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        Img img = imgService.storeFile(auctionDTO.getFile(), user.getId(), CATEGORY);
        auctionDTO.setImageId(Long.parseLong(img.getId()));

        return makeNewAuction(auctionDTO, user);
    }

    public Auction makeNewAuction(AuctionDTO auctionDTO, User user) {
        Auction auction = makeNewAuctionObject(auctionDTO, user.getId(), user.getLogin());
        UserBid userBid = userBidService.makeInitialUserBid(auction);
        userBidService.save(userBid);
        setInitialUserBid(auction, userBid);

        saveNewAuction(auction);
        auctionDTO.setOwnerName(user.getLogin());

        return auction;
    }

    @Transactional
    public Auction createNewAuctionAngularVersion(AuctionDTO auctionDTO, Long userId) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        return makeNewAuction(auctionDTO, user);
    }

    private void setInitialUserBid(Auction auction, UserBid userBid) {
        List<UserBid> userBidsUpdated = auction.getUserBids();
        userBidsUpdated.add(userBid);

        save(auction);
    }

    public Auction save(Auction auction) {
        return auctionRepository.save(auction);
    }

    public List<Auction> returnAllAuctions() {
        return auctionRepository.findAll();
    }

    public Optional<List<Auction>> findByTypeOfCharacterAndTypeOfImage(String type, String category) {
        return auctionRepository.findByTypeOfCharacterAndTypeOfImage(type, category);
    }

    private boolean isThisBidLowerThenAutoBuy(BigDecimal newBidValue, BigDecimal autoBuyValue) {
        int result = newBidValue.compareTo(autoBuyValue);
        if (autoBuyValue.compareTo(new BigDecimal(0)) == 0) {
            return true;
        }
        return result < 0;
    }

    private Auction endAuctionByAutoBuy(Auction auction, UserBid newUserBid) {
        List<UserBid> userBidsUpdated = auction.getUserBids();
        userBidsUpdated.add(newUserBid);
        userBidService.save(newUserBid);

        auction.setAuctionOverByAutoBuy(true);
        return save(auction);
    }
}
