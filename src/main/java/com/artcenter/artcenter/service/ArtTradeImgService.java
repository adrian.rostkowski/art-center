package com.artcenter.artcenter.service;

import com.artcenter.artcenter.exception.FileStorageException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.ArtTradeImg;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.repositories.ArtTradeImgRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Service
public class ArtTradeImgService {
    private final ArtTradeImgRepository artTradeImgRepository;
    private final UserService userService;

    public ArtTradeImgService(ArtTradeImgRepository artTradeImgRepository, UserService userService) {
        this.artTradeImgRepository = Objects.requireNonNull(artTradeImgRepository,
                "artTradeImgRepository must be not null");
        this.userService = Objects.requireNonNull(userService,
                "userService must be not null");
    }

    public Optional<ArtTradeImg> getById(Long id) {
        return artTradeImgRepository.findById(id);
    }

    public ArtTradeImg storeFile(MultipartFile file, Long userId, String category) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            ArtTradeImg img = ArtTradeImg.builder()
                    .fileName(fileName)
                    .fileType(file.getContentType())
                    .data(file.getBytes())
                    .ownerId(userId)
                    .ownerName(user.getLogin())
                    .category(category)
                    .build();

            return artTradeImgRepository.save(img);
        } catch (IOException ex) {
            throw new FileStorageException(fileName, ex);
        }
    }
}
