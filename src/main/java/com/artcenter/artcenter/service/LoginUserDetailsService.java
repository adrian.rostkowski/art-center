package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.LoginUser;
import com.artcenter.artcenter.model.Role;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.repositories.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LoginUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public LoginUserDetailsService(UserRepository userRepository) {
        this.userRepository = Objects.requireNonNull(userRepository, "userRepository must be not null");
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<User> userOpt = userRepository.findByLogin(login);

        if (userOpt.isPresent() && userOpt.get().getActive() == 1) {
            Set<Role> roles = userOpt.get().getRoles();
            return new LoginUser(userOpt.get(), getAuthorities(roles));
        }
        throw new UsernameNotFoundException(login);
    }

    private List<GrantedAuthority> getAuthorities(Set<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }
}
