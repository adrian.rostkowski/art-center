package com.artcenter.artcenter.service;


import com.artcenter.artcenter.exception.FileStorageException;
import com.artcenter.artcenter.exception.UserNotFoundException;
import com.artcenter.artcenter.model.Img;
import com.artcenter.artcenter.model.User;
import com.artcenter.artcenter.model.UserAvatar;
import com.artcenter.artcenter.repositories.DBFileRepository;
import com.artcenter.artcenter.repositories.UserAvatarRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ImgService {

    //todo load this from prop. file
    private static final String IMG_CATEGORY_GALLERY = "gallery";
    private final DBFileRepository dbFileRepository;
    private final UserService userService;
    private final UserAvatarRepository userAvatarRepository;

    public ImgService(DBFileRepository dbFileRepository, UserService userService, UserAvatarRepository userAvatarRepository) {
        this.dbFileRepository = Objects.requireNonNull(dbFileRepository, "dbFileRepository must be not null");
        this.userService = Objects.requireNonNull(userService, "userService must be not null");
        this.userAvatarRepository = Objects.requireNonNull(userAvatarRepository, "userAvatarRepository must be not null");
    }

    public Img storeFile(MultipartFile file, Long userId, String category) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            Img img = Img.builder()
                    .fileName(fileName)
                    .fileType(file.getContentType())
                    .data(file.getBytes())
                    .ownerId(userId)
                    .ownerName(user.getLogin())
                    .category(category)
                    .build();

            return dbFileRepository.save(img);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public UserAvatar storeUserAvatar(MultipartFile file, Long userId) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", String.valueOf(userId)));

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            UserAvatar userAvatar = UserAvatar.builder()
                    .data(file.getBytes())
                    .user(user)
                    .build();

            user.setUserAvatar(userAvatar);

            return userAvatarRepository.save(userAvatar);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Optional<Img> getFile(String fileId) {
        return dbFileRepository.findById(fileId);
    }

    public Optional<List<Img>> returnAllGalleryImages(Long userId) {
        return dbFileRepository.findByCategoryAndOwnerId(IMG_CATEGORY_GALLERY, userId);
    }
}
