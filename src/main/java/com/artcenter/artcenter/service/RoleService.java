package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.Role;
import com.artcenter.artcenter.repositories.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = Objects.requireNonNull(roleRepository, "roleRepository must be not null");
    }

    public Optional<Role> findByName(String name) {
        return roleRepository.findByName(name);
    }
}
