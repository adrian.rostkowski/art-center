package com.artcenter.artcenter.service;

import com.artcenter.artcenter.exception.BindingResultValidException;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

@Log4j
@Service
public class BindingResultService {

    public void logBindingResultErrors(List<ObjectError> objectErrors) {
        objectErrors.forEach(error -> log.debug(String.format("Object name : [%s], error message : [%s]",
                error.getObjectName(), error.getDefaultMessage())));
    }

    public void throwBindingResultValidException(BindingResult bindingResult) {
        String errorMessage = "";
        List<ObjectError> objectErrors = bindingResult.getAllErrors();

        for(ObjectError error : objectErrors) {
            errorMessage += error.getObjectName() + " " + error.getDefaultMessage()  + "\n";
        }

        throw new BindingResultValidException(errorMessage);
    }
}
