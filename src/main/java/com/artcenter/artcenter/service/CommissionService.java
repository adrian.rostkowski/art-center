package com.artcenter.artcenter.service;

import com.artcenter.artcenter.model.Auction;
import com.artcenter.artcenter.model.Commission;
import com.artcenter.artcenter.repositories.CommissionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CommissionService {

    private final CommissionRepository commissionRepository;

    public CommissionService(CommissionRepository commissionRepository) {
        this.commissionRepository = Objects.requireNonNull(commissionRepository,
                "commissionRepository must be not null");
    }

    public Optional<List<Commission>> findByArtistName(String artistName) {
        return commissionRepository.findByArtist(artistName);
    }

    //todo should find by winner id in relation many to many
    public Optional<List<Commission>> findByWinnerName(String winner) {
        return commissionRepository.findByWinner(winner);
    }

    public Optional<Commission> findById(Long id) {
        return commissionRepository.findById(id);
    }

    public Commission save(Commission commission) {
        return commissionRepository.save(commission);
    }

    public Commission makeNewCommission(Auction auction) {
        String winner = auction.getUserBids().get(auction.getUserBids().size() - 1).getBidOwner();

        Commission commission = Commission.builder()
                .artist(auction.getOwnerName())
                .winner(winner)
                .isActive(true)
                .auction(auction)
                .build();

        return save(commission);
    }
}
