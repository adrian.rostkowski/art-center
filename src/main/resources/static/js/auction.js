function debtTimer(sec, elementId) {
    var x = setInterval(function () {

        var numberOfDays = Math.floor(sec / 86400);
        var numberOfHours = Math.floor((sec % 86400) / 3600);
        var numberOfMinutes = Math.floor(((sec % 86400) % 3600) / 60);
        var numberOfSeconds = Math.floor(((sec % 86400) % 3600) % 60);

        console.log(elementId + " " + sec + " " + isNaN(sec));

        document.getElementById(elementId).innerHTML = numberOfDays + "d "
            + numberOfHours + "h " + numberOfMinutes + "m " + numberOfSeconds + "s ";

        if (sec <= 0 || sec === " ") {
            if (sec === " ") {
                document.getElementById(elementId).innerHTML = " ";
                return
            }
            document.getElementById(elementId).innerHTML = "EXPIRED";
            clearInterval(x);
        }
        sec--;
    }, 1000);
}